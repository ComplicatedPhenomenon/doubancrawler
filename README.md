* [使用示例](#使用示例)
* [获取更多信息的思路](#增加新功能的思路举例)
* [值得一做的算法方向](#值得一做的算法方向)
    * [分析挖掘到的文本内容](#分析挖掘到的文本内容)
    * [使用开源工具进行初步试验](#百度AI开放平台)
* [工程上需要改进的地方](#工程上需要改进的地方)
* [使用python语言的高级用法](#Python语言的高级用法)
* [一些经验](#Misc)

## 使用示例
这个库提供两个主要应用程序 `crawler.py`和`test_new_features.py`。另外有一些功能不提供公开调用，鼓励进行手动尝试。
### `crawler.py`

(1) Crawl all related douban groups
```
python3 crawler.py --type douban_group_list
```

(2) Filter the douban groups and limit the number of groups to crawl
```
python3 filter.py --type douban_group_list --limit 3  --save
```

(3) Crawl all discussions in the first page of the groups in the allowlist
```
python3 crawler.py --type douban_discussion_list --limit 1
```

(4) Filter discussion by hard coded rules
```
python3 filter.py --type douban_discussion_list
```
### `test_new_features.py`

```
python3 test_new_features.py --help
```
## 增加新功能的思路举例
### 思考要获取哪些信息

我关心的是话题下面的回复， 以及涉及到的用户，再去获取该用户的主页内容，包括日记等


使用浏览器驱动来模拟人操作网页`chromedriver + selenium + beautifulsoup`

定位提取页面中的信息，HTML被解析（parse）成一个数据结构， 比如tree，然后应用这种数据结构相应的方法提取信息。


从下面举例的页面html源码来看，它还是很丰富的，元素比较多
```html
<h4>
<a class="" href="https://www.douban.com/people/168222805/">浅空无素</a>
<span class="pubtime">2019-06-29 16:28:16</span>
</h4>
</div>
<p class="reply-content">喜欢甜甜的故事</p>
<div class="operation_div" id="168222805">
<div class="operation-more">
<a class="lnk-delete-comment" data-cid="1965723034" href="javascript:void(0);" rel="nofollow" title="真的要删除浅空无素的发言?">删除</a>
...
<div class="reply-doc content" style="padding-left:0px;">
<div class="bg-img-green">
```
实际上每个页面不需要下载下来分析确定元素定位，只需要打开浏览器控制台查看即可。

 
## 值得一做的算法方向

### 分析挖掘到的文本内容
收集这么多的中文文本，文本分析还需要继续做。

对一个丰富的小组话题，可以做
* 抽取出事件, 时间线上的事件。

    比如一个感情帖，需要提取出最近遇上什么事， 遇见了有意思的人，靠近，停留，分手等这些标志性事件

* 对每条回复进行分类(打标签)，比如：
    * 经验交流
    * 情感建议
    * 语气词，冒个泡
    * 有感而发，(篇幅较长)
    * 批判
    * 鼓励
    * 催更
    * 调侃

对个人信息，可以进行
* 社交关系分析

    随着时间，与你社交的人活跃度在变化，这个怎么动态展示。一开始，一个用户就像是一个孤岛，与外界没有互动，随着用户公开的行为，比如生产内容，评论，就会有可能被别人注意到，然后就与这个世界进行互动

* 对文本提取结构化数据。人物，社交关系，性格，成长环境，职业，价值取向，感情观， 职位，习惯


* https://github.com/fighting41love/funNLP
* https://zhuanlan.zhihu.com/p/143767696

### 百度AI开放平台
* 找适用这个情景的算法

    语言处理应用技术接口能力 情感倾向分析， 对话情绪识别， 评论观点抽取
    * API接口 https://ai.baidu.com/tech/nlp/sentiment_classify

    * 调用接口的方法 https://ai.baidu.com/ai-doc/NLP/tk6z52czc
* 获得授权
  1. 通过查看注册应用的 API Key & Secret Key https://console.bce.baidu.com/ai/?locale=zh-cn#/ai/nlp/app/detail~appId=2108748

**试验结论**: 不满意，算法给出的结果准确度差强人意，提取到的信息意义不大。简短的正式文本识别还行，过于口语话和较长文本结果不靠谱。

## 工程上需要改进的地方
* For now, all filter rules are hard coded. Next step is to make it configurable.
* Add logout function 
* Speed up the crawling process
* Set up proxy to avoid being prevented by `Douban` server

    http://www.daimacode.com/python/1879.htm

    没有找到免费的可靠代理池来简单地测试一下。

## Python语言的高级用法
### multiple inheritance
the `__mro__` attribute is only visible on the class rather than the instance. (Method Resolution Order, 方法解析顺序)

```sh
DoubanProfileStatus_2.mro()
[<class 'crawlers.DoubanProfile.DoubanProfileStatus_2'>, <class 'crawlers.MultiPagesCrawler.MultiPagesCrawler'>, <class 'crawlers.DoubanProfile.SaveProfileInfo'>, <class 'object'>]
```

Python looks for each attribute in the class's parents as they are listed left to right. 
* https://stackoverflow.com/a/36780883/7583919

what will happen when parent class has no `__init__` method?

when child instance has no attribute of parent attribute
```sh
dir(DoubanProfileStatus_2)
```
* https://stackoverflow.com/a/39415209/7583919
* https://stackoverflow.com/a/33469090/7583919

```sh
(Pdb) DoubanProfileStatus.__mro__
(<class 'crawlers.DoubanProfile.DoubanProfileStatus'>, <class 'crawlers.MultiPagesCrawler.MultiPagesCrawler'>, <class 'crawlers.DoubanProfile.SaveProfileInfo'>, <class 'crawlers.DoubanProfile.RegularAction'>, <class 'object'>)
```

https://stackoverflow.com/q/65307778/7583919
```py
class DoubanProfileStatus(MultiPagesCrawler, SaveProfileInfo, RegularAction):
    def __init__(self, url, browser, max_pages, dir_path, file_name):
        print('initialize DoubanProfileStatus')
        super().__init__(url, browser, max_pages, dir_path, file_name)
        self.old_info = []
        self.new_info = []
```

```
TypeError: __init__() takes 4 positional arguments but 6 were given
```

### decorator 
```py
def timeConsumed(a_func):
    def wrapTheFunction(*args, **kwargs):
        a = time.time()
        res = a_func(*args, **kwargs)
        b = time.time()
        print(f'{a_func.__name__} takes {round(b-a, 1)} seconds')
        return res
    return wrapTheFunction

@timeConsumed
a_func(*args, **kwargs):
    ... 
``` 
`a_func(*args, **kwargs)` equals to `timeConsumed(a_func)(*args, **kwargs)` the execution starts from left to right, first it returns an object `wrapTheFunction`, which is a function, then the function is being called right away.



### Misc
* 定位文件夹

    不小心把一个目录移动到未知的位置, command `find` save my life.
    ```sh
    ± |master U:3 ✗| → find ~/Playground/ -type d -name 'douban_topic_reply'
    ```

* 数据结构的设计

    Douban 个人主页里的广播， 每一条内容会有回复，每一个回复都可能涉及引用， 树的数据结构存在数组里面

    `[reply1, quote: reply0] [reply2, quote: None] [reply3, quote: reply1]`

    这样最后也能找到完整的链条。


* 优化过长topic多次获取时的方式

    有的话题非常的长，评论条数过万，也就是100页+，如果多次跟新这个页面，每次都从最早的评论进行时间戳比对，难免效率低下，因为话题比如有两百页的评论，这次更新是在你上次获取的基础上更新了三页，如果中间没有大规模删除，你就还得白白翻两百页。

    和个人主页不同，个人主页是从最新到最早，话题的评论是从最早到最晚。如果你选择把上次获取时最晚的页码永久保留下来，基于现有的JSON数据结构，需要另存一个文件，这不可取。

    实际上话题的首页已经包含了最新的页面信息，去到这一页然后倒着比
    '''
    soup.find('div', class_='paginator').find_all('a')[1]['href']
    'https://www.douban.com/group/topic/193711599/?start=200'
    '''