# python3.9 -m streamlit run app/main.py
import json
import os, sys
from typing import Dict, List

import numpy as np
import pandas as pd
import plotly.figure_factory as ff
import streamlit as st

# sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from filters import raw_data_processing
from filters import clean_group_topic, ZuFang
from misc.constants import LOGGER_FORMAT, CRAWLER_TYPE, CHROME_DRIVE_PATH, CRAWLER_FOLDER
from misc.utils import logger, timeConsumed, login, LoginBySession


data = json.load(open('config.json', 'r'))
st.header('豆瓣数据可视化')
st.subheader('北京租房')


def data_analysis_on_group_zufang(group_places_dict:Dict[str, List]):
    for group, places in group_places_dict.items():
        
        zf = ZuFang(group, places)
        st.caption('原始数据')
        st.dataframe(zf.df)
        ps = zf.df.groupby(pd.Grouper(key='ts', freq='H')).size()
        fig = zf.plot_ps(ps, '', use_with_streamlit=True)
        st.pyplot(fig)
        ps = zf.compose_num_vs_place()
        fig = zf.plot_ps(ps, '', use_with_streamlit=True)
        st.pyplot(fig)
        

group_places_dict = data['data_analysis_on_group_zufang']

data_analysis_on_group_zufang(group_places_dict)

st.caption('可视化结果')