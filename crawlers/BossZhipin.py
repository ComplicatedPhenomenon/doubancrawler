from datetime import datetime
import json
import logging
import os
import sys
from typing import Optional
from time import sleep
from concurrent.futures import ProcessPoolExecutor, as_completed

from bs4 import BeautifulSoup
from pathlib import Path
import requests
from selenium.common.exceptions import NoSuchElementException

from .MultiPagesCrawler import MultiPagesCrawler
from misc.utils import timeConsumed, logger, handleTimeout, pass_this_saying


class BossZhipin(MultiPagesCrawler):
    def __init__(self, url, browser, max_pages):
        super().__init__(url=url, browser=browser, max_pages=max_pages)
        self.info={}

    def process_page(self, soup) -> bool:
        # self.browser.find_elements_by_class_name('menu-article')[0].find_elements_by_xpath('//div[@class="text"]/a')[0].get_attribute('href')
        jobs = self.browser.find_elements_by_xpath('//div[@class="job-primary"]')
        breakpoint()
        while True:
            try:
                for job in jobs:
                    name_link = job.find_element_by_xpath('//span[@class="job-name"]/a')
                    job_link = name_link.get_attribute('href')
                    title = name_link.get_attribute('title')
                    area = job.find_element_by_xpath('//span[@class="job-area"]').text
                    salary = job.find_element_by_xpath('//span[@class="red"]').text
                    tags = job.find_elements_by_xpath('./div/div[@class="tags"]')[0].text
                    self.info[job_link] = (title, salary, tags, area)
                self.browser.find_element_by_class_name('next').click()
            except NoSuchElementException:
                breakpoint()

        breakpoint()
        job_details = list(self.info.keys())
        for url in job_details:
            self.browser.get(url)
            company_info = self.browser.find_element_by_class_name('sider-company').text
            job_desc = self.browser.find_elements_by_xpth('//div[@class="detail-content"]/div[@class="job-sec"]')
            breakpoint()
            self.info[url] += (company_info, job_desc)
        return True