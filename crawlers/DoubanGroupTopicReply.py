#!/usr/bin/python
# -*- coding: UTF-8 -*-
import csv
from datetime import datetime
import json
import logging
import os
import sys
from typing import Optional
from time import sleep
from concurrent.futures import ProcessPoolExecutor, as_completed

from bs4 import BeautifulSoup
from pathlib import Path
import requests
from selenium.common.exceptions import NoSuchElementException

from .MultiPagesCrawler import MultiPagesCrawler
from misc.utils import timeConsumed, logger, handleTimeout, pass_this_saying


class SaveTopicReply():
    def __init__(self, dir_path=None, file_name=None, **kwargs):
        super().__init__(**kwargs)
        self.dir_path = dir_path
        self.file_name = file_name if file_name else self._get_file_name()
        self.old_topic_reply = {'comments': []}
        self.collections = None
        self.latest_history = self._check_latest_history()
        self.picture_urls = []


    def _check_latest_history(self):
        '''
        prevent history being deleted later on
        '''
        history_file_1 = f'{self.dir_path}/{self.file_name}/{self.file_name}.json'
        flag_1 = os.path.isfile(history_file_1)
        if flag_1:
            try:
                with open(history_file_1, 'r') as f:
                    self.old_topic_reply = json.load(f)
                latest_history = self.old_topic_reply['comments'][-1]['reply_time']
                logger.info(f'only need to get infos late than {latest_history}')
                return latest_history
            except Exception as e:
                logger.info('No history burden')
                return None


    def _extract_pic(self, pic_url, comment_time):
        self.picture_urls.append({comment_time: pic_url})
        file_path = f'{comment_time}.jpg'
        return file_path


    def save(self, only_csv=False) -> bool:
        Path(f'{self.dir_path}/{self.file_name}').mkdir(parents=True, exist_ok=True)
        if only_csv:
            with open(f'{self.dir_path}/{self.file_name}/{self.file_name}.csv', 'a', newline='', encoding='utf-8') as f:
                write = csv.writer(f, delimiter=';')
                write.writerows(self.collections)
            return True
        if (len(self.topic_reply['comments']) != 0)  & (len(self.old_topic_reply['comments']) != 0):
            self.old_topic_reply['comments'] += self.topic_reply['comments']
            logger.info('merge history data with new data')
            info = self.old_topic_reply

        elif (len(self.old_topic_reply['comments']) == 0):
            logger.info('no history data, only new data')
            info = self.topic_reply
        else:
            info = {}

        if len(info) != 0:
            with open(f'{self.dir_path}/{self.file_name}/{self.file_name}.json', 'w') as outfile:
                json.dump(info, outfile, ensure_ascii=False, indent=2)

        for item in self.picture_urls:
            for comment_time, pic_url in item.items():
                img_data = requests.get(pic_url).content
                with open(f'{self.dir_path}/{self.file_name}/{comment_time}.jpg', 'wb') as handler:
                    handler.write(img_data)
        return True


class DoubanTopicReply(MultiPagesCrawler, SaveTopicReply):
    """
    使用BeautifulSoup提取小组话题数据
    """
    def __init__(self, url, browser, max_pages, dir_path, file_name):
        super().__init__(url=url, browser=browser, max_pages=max_pages, \
                         dir_path=dir_path, file_name=file_name)
        self.topic_reply = {'comments': []}
        self.info_has_been_used = False


    def _find_starting_page(self, soup):
        page_num = soup.find('div', class_='paginator').find_all('a')[-2].text
        page_num = int(page_num)
        while page_num > 0:
            start = (page_num-1)*100
            url = self.url + f'?start={start}'
            flag, page = self.fetch(url)
            if not flag:
                break
            soup = BeautifulSoup(page['page_source'], "html.parser")
            flag = self._is_this_page_the_starting_page(soup)
            if flag:
                logger.info(f'latest history comment is on page {page_num+1}')
                return url
            page_num -= 1


    def next_url(self, soup) -> Optional[str]:
        if self.latest_history and not self.info_has_been_used:
            self.info_has_been_used = True
            url =self._find_starting_page(soup)
            return url

        next_span = soup.body.find('span', class_='next')
        if next_span:
            next_link = next_span.find('a')
            if next_link:
                return next_link['href']
        return None


    def _is_this_page_the_starting_page(self, soup) -> bool:
        replies = soup.find_all('div', class_='reply-doc content')
        flag, reply_info = self.extract_comment(replies[0])
        return False if flag else True


    def process_page(self, soup) -> bool:
        '''
        抽取出话题下面的信息
        '''
        comments = []
        replies = soup.find_all('div', class_='reply-doc content')
        for reply in replies:
            flag, reply_info = self.extract_comment(reply)
            if flag:
                print('fetch!')
                comments.append(reply_info)
            else:
                print('Pass!')

        if soup.find(['h3']):
            title_ = soup.find('div', class_='rich-content topic-richtext')
            title = title_.text.strip('\n') if title_ else ''
            published_time = soup.find(['h3']).find('span', class_='create-time').text
            author_name = soup.find(['h3']).find('span', class_='from')\
                              .text.strip('\n').strip('来自: ')
            author_id =  soup.find('span', class_='from').find('a').get('href')
            
            self.topic_reply = {'title':title, 
                                'published_time': published_time, 
                                'author_name': author_name,
                                'author_id': author_id, 
                                'comments': comments}
        else:
            self.topic_reply['comments'] += comments
        return False


    def extract_comment(self, reply):
        reply_time = reply.find('span', class_='pubtime').text
        if pass_this_saying(self.latest_history, reply_time):
            return False, None

        reply_id = reply.find('a')['href']
        reply_name = reply.find('a').text
        reply_content = reply.find('p', class_='reply-content').text
        fig_path = ''
        if reply.find('img'):
            fig_url = reply.find('img')['src']
            fig_path = self._extract_pic(fig_url, reply_time)

        quote = {'quote_content': '', 'quote_author': ''}
        zed = reply.find('div', class_='reply-quote-content')
        if zed:
            quote['quote_content'] = zed.find('span', class_='all').text
            zed_1 = zed.find('span', class_='pubdate')
            quote['quote_author'] = zed_1.find('a')['href'] if zed_1 else '异常'
        
        res = {'reply_name': reply_name, 
              'reply_id': reply_id, 
              'reply_time': reply_time, 
              'reply_content': reply_content,
              'fig_path': fig_path, 
              'quote': quote}
        return True, res


class DoubanTopicReply_2(SaveTopicReply):
    """
    使用xpath提取小组话题数据
    """
    def __init__(self, url, browser, max_pages, dir_path, file_name):
        super().__init__(dir_path=dir_path, file_name=file_name)
        self.url = url
        self.browser = browser
        self.max_pages = max_pages
        self.topic_reply = {'comments': []}


    def _find_starting_page(self):
        try: 
            page_num= self.browser.find_element_by_xpath('//div[@class="paginator"]/span[@class="thispage"]').get_attribute('data-total-page')
        except NoSuchElementException:
            breakpoint()

        page_num = int(page_num)
        while page_num > 0:
            start = (page_num-1)*100
            url = self.url + f'?start={start}'
            self.get_url(url)
            flag = self._is_this_page_the_starting_page()
            if flag:
                logger.info(f'latest history comment is on page {page_num}')
                return url
            page_num -= 1


    def _is_this_page_the_starting_page(self) -> bool:
        first_comment_webelement = self.browser.find_element_by_xpath('//ul[@class="topic-reply"][@id="comments"]/li[1]')
        this_comment_time = first_comment_webelement.find_element_by_xpath('./div[@class="reply-doc content"]/div/h4/span[@class="pubtime"]').text
        return self.latest_history >= this_comment_time 


    def process_pages(self) -> bool:
        '''
        由当前的页面开始, 不断跳转到下一页，直至最后一页， 提取每个页面上的信息
        '''
        current_page = self.browser.find_element_by_xpath\
                       ('//div[@class="paginator"]/span[@class="thispage"]').text
        current_page = int(current_page)
        if current_page==1:
            title = self.browser.find_element_by_tag_name('h1').text
            x = self.browser.find_element_by_class_name('topic-doc')
            author_name = x.find_element_by_xpath('./h3/span[1]/a').text
            author_id = x.find_element_by_xpath('./h3/span[1]/a').get_attribute('href')
            published_time = x.find_element_by_xpath('./h3/span[2]').text
            topic_body = x.find_element_by_xpath('./div/div/div').text
            self.topic_reply = {'title':title,
                                'published_time': published_time, 
                                'author_name': author_name,
                                'author_id': author_id, 
                                'topic_body': topic_body}

        comments_ls = []
        # total_pages= self.browser.find_element_by_xpath('//div[@class="paginator"]/span[@class="thispage"]').get_attribute('data-total-page')
        # total_pages = int(total_pages)

        # 提取当前页面的评论条目
        while True:
            try:
                current_page = self.browser.find_element_by_xpath\
                    ('//div[@class="paginator"]/span[@class="thispage"]').text
            except NoSuchElementException:
                print('OMG current_page')
                breakpoint()
            current_page = int(current_page)
            start = (current_page-1)*100
            url = self.url + f'?start={start}'
            logger.info(f'processing page {current_page}')
            comments_ls += self.extract_a_page()
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
            except NoSuchElementException:
                break

        item_name = ('reply_name', 'reply_id', 'reply_time', 'reply_content', 'fig_path', 'quote')
        comments_ls = [dict(zip(item_name, i)) for i in comments_ls]
        self.topic_reply['comments']+= comments_ls


    @timeConsumed
    def extract_a_page(self):
        comments_ls = []
        comment_elements = self.browser.find_elements_by_xpath\
            ('//ul[@class="topic-reply"][@id="comments"]/li')
        start=-1
        for i, comment in enumerate(comment_elements):
            reply_time = comment.find_element_by_xpath\
                ('./div[@class="reply-doc content"]/div/h4/span[@class="pubtime"]').text
            if reply_time > self.latest_history:
                start = i
                break
        # 凡是会调用到extract_a_page 这个函数的页面， 在self.latest_history 不为none时， 要么均比历史新，要么部分比历史新， 要么最后一个即是最新历史
        # 第一种情况代表所有的元素
        if start==-1:
            return comments_ls

        for i, comment in enumerate(comment_elements[start:], start):
            reply_time = comment.find_element_by_xpath\
                ('./div[@class="reply-doc content"]/div/h4/span[@class="pubtime"]').text
            reply_name = comment.find_element_by_xpath\
                ('./div[@class="reply-doc content"]/div/h4/a').text
            reply_id = comment.find_element_by_xpath\
                ('./div[@class="reply-doc content"]/div/h4/a').get_attribute('href')
            reply_content = comment.find_element_by_xpath\
                ('./div[@class="reply-doc content"]/p[@class=" reply-content"]').text
            fig_path = ''
            try:
                fig_path =  comment.find_element_by_xpath\
                    ('./div[@class="reply-doc content"]/div/div/img').get_attribute('data-orig')
            except NoSuchElementException:
                pass
            except Exception:
                breakpoint()
                
            quote = {'quote_content': '', 'quote_author': ''}
            try:
                quote['quote_content'] = comment.find_element_by_xpath('./div/div/div/span[@class="short"]').text
                quote['quote_author'] = comment.find_element_by_xpath('./div/div/div/span[@class="pubdate"]/a').get_attribute('href')
            except NoSuchElementException:
                pass
            except Exception:
                breakpoint()
            comments_ls.append((reply_name, reply_id, reply_time, reply_content, fig_path, quote))
            print(f'fetch {i}')
        return comments_ls


    def extract_comments_on_this_page(self, comment, i):
        reply_time = comment.find_element_by_xpath('./div[@class="reply-doc content"]/div/h4/span[@class="pubtime"]').text
        reply_name = comment.find_element_by_xpath('./div[@class="reply-doc content"]/div/h4/a').text
        reply_id = comment.find_element_by_xpath('./div[@class="reply-doc content"]/div/h4/a').get_attribute('href')
        reply_content = comment.find_element_by_xpath('./div[@class="reply-doc content"]/p[@class=" reply-content"]').text
        fig_path = ''
        try:
            fig_path =  comment.find_element_by_xpath('./div[@class="reply-doc content"]/div/div/img').get_attribute('data-orig')
        except NoSuchElementException:
            pass
        quote = {'quote_content': '', 'quote_author': ''}
        try:
            quote['quote_content'] = comment.find_element_by_xpath('./div/div/div/span[@class="short"]').text
            quote['quote_author'] = comment.find_element_by_xpath('./div/div/div/span[@class="pubdate"]/a').get_attribute('href')
        except NoSuchElementException:
            pass
        a_comment = (reply_name, reply_id, reply_time, reply_content, fig_path, quote)
        print(f'fetch {i}')
        return a_comment
    

    @handleTimeout
    def get_url(self, url):
        self.browser.get(url)
    

    def get_collections(self):
        self.browser.find_element_by_link_text('收藏').click()
        res = []
        while True:
            page_source = self.browser.page_source
            soup = BeautifulSoup(page_source, "html.parser")
            x = soup.find('div', class_='list collect-list').find_all('a', href=True)
            if not x:
                break
            y = soup.find('div', class_='list collect-list').find_all('span')
            ids = [i.text for i in x[1::3]]
            ids_links = [i['href'] for i in x[1::3]]
            timestamps = [i.text for i in y]
            bookmarks = [i.text for i in x[2::3]]
            bookmarks_links = [i['href'] for i in x[2::3]]
            assert len(ids)==len(ids_links)==len(timestamps)==len(bookmarks)==len(bookmarks_links)
            zed = tuple(zip(timestamps, ids, ids_links, bookmarks, bookmarks_links))
            res.extend(zed)
            try:
                self.browser.find_element_by_link_text('后页>').click()
                sleep(0.2)
            except NoSuchElementException:
                breakpoint()
            except Exception as e:
                print(e)
        self.collections = res
        self.save(only_csv=True)


    def start(self):
        self.get_url(self.url)
        # self.get_collections()
        self.get_url(self.url)
        # 依据历史信息，确定起始页面
        if self.latest_history:
            start_page_url = self._find_starting_page()
            self.get_url(start_page_url)
        self.process_pages()