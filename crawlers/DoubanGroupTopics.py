import time

from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor, as_completed
import requests
import pickle
from pathlib import Path
from selenium.common.exceptions import NoSuchElementException

from .MultiPagesCrawler import CrawlerBase
from misc.utils import timeConsumed, logger
from misc.constants import LOGGER_FORMAT, CRAWLER_TYPE, USER_AGENT
from misc.utils import timeConsumed, logger, handleTimeout

# def get_proxy():
#     return requests.get('http://127.0.0.1:5010/get/').json()

# def delete_proxy(proxy):
#     requests.get(f'http://127.0.0.1:5010/delete/?proxy={proxy}')

class DoubanGroupTopics():
    """
    用BeautifulSoup提取小组信息
    """
    def __init__(self, group_name, max_pages, dir_path):
        self.group_name = group_name
        self.max_pages = max_pages
        self.dir_path = dir_path
        self.headers = {'User-Agent': USER_AGENT}

    @timeConsumed
    def get_topics(self, parallel=False):
        '''
        Decide what kind of topic worth taking a further look
        '''    
        
        base_url = f'https://www.douban.com/group/{self.group_name}/discussion?start='
        # # https://github.com/jhao104/proxy_pool/issues/406
        # retry_count = 5
        # proxy = get_proxy().get('proxy')
        # breakpoint()
        # resp = requests.get(base_url + '0', headers=self.headers)
        # while retry_count > 0:
        #     try:
        #         resp = requests.get(base_url + '0', headers=self.headers,
        #                             proxies={'http': f'http://{proxy}'})
        #         if resp.status_code == 200:
        #             logger.info('ready to go')
        #             break
        #         else:
        #             logger.info(f'you might be blocked as you get {resp.status_code}')
        #     except Exception:
        #         retry_count -= 1
        # breakpoint()
        resp = requests.get(base_url + '0', headers=self.headers)
        if resp.status_code == 200:
            logger.info('ready to go')
        else:
            logger.info(f'you might be blocked as you get {resp.status_code}')
            breakpoint()
            return

        # SET UP MAXIMUM THREADS NUMBER 
        data = []
        params =[page*25 for page in range(self.max_pages)]
        futures = []
        with ThreadPoolExecutor(120) as executor:
            for page, param in enumerate(params):
                futures.append(executor.submit(self._fetch_page, 
                        base_url, page, param))
        
        for task in as_completed(futures):
            status_code, topic_detail_per_page = task.result()
            if status_code != 200:
                logger.info(f'A task has status_code as: {status_code}')
            data += topic_detail_per_page
        breakpoint()
        self.save(data)


    def _fetch_page(self, base_url, page, param):
        logger.info(f'fetching page {page}')
        time.sleep(1)
        try:
            resp = requests.get(base_url + f'{param}', headers=self.headers)
            cnt = 0
            while (resp.status_code!=200 and cnt < 10):
                resp = requests.get(base_url + f'{param}', headers=self.headers)
                cnt += 1
                time.sleep(0.5)
            if resp.status_code==200:
                soup = BeautifulSoup(resp.text, 'html.parser')
                topics = soup.find_all('tr', class_='')
                topic_detail_per_page = self._extract_topics(topics)
                logger.info(f'{resp.status_code}')
                return 200, topic_detail_per_page
            else:
                logger.info(f'{resp.status_code}')
                return resp.status_code, []

        except Exception as e:
            logger.info(e)
            return 400, []


    def _extract_topics(self, topics):
        topic_detail = []
        for topic in topics:
            zed = topic.find_all('a', class_='')
            response_num = topic.find('td', class_='r-count').text
            latest_response_time = topic.find('td', class_='time').text
            item = (zed[0]['href'],  zed[0]['title'], zed[1]['href'], zed[1].text,
                    response_num, latest_response_time)
            topic_detail.append(item)
        return topic_detail


    def save(self, data):
        Path(f'{self.dir_path}/{self.group_name}').mkdir(parents=True, exist_ok=True)
        file_name = f'{self.dir_path}/{self.group_name}/{self.group_name}.pkl'
        pickle.dump(data, open(file_name, 'wb'))


class DoubanGroupTopics_2(CrawlerBase):
    """
    用xpath提取小组信息
    """

    def __init__(self, url, browser, start_page, max_pages, dir_path, file_name):
        super().__init__(url=url, browser=browser, \
                         dir_path=dir_path, file_name=file_name)
        self.max_pages = max_pages
        self.start_page = start_page
        self.latest_history = None


    def check_last_fetch(self):
        # 找到一个文件， 这个文件保存了最后一次抓取的数据， 读取第一条数据的时间戳
        target_path = f'{self.dir_path}/{self.file_name}'
        p = Path(target_path)
            


    @timeConsumed
    def process_pages(self) -> bool:
        '''
        由当前的页面开始, 不断跳转到下一页，直至最后一页， 提取每个页面上的信息
        '''
        current_page = int(self.browser.find_element_by_xpath('//div[@class="paginator"]/span[@class="thispage"]').text)
        while current_page < self.max_pages:
            try:
                current_page = self.browser.find_element_by_xpath('//div[@class="paginator"]/span[@class="thispage"]').text
            except NoSuchElementException:
                logger.info('OMG! unexpected behavior')
                self.save()
                breakpoint()
                
            current_page = int(current_page)
            if current_page // 100 == 0: 
                time.sleep(0.9)
            start = (current_page-1)*25
            url = self.url.format(start)
            logger.info(f'processing page {current_page}')
            zed = self.extract_a_page()
            self.data += zed 
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
            except NoSuchElementException:
                self.get_url(self.url.format(start))
                self.save()
                logger.info('Reach last page')
                break


    # @timeConsumed
    # def extract_a_page(self):
    #     items = self.browser.find_elements_by_xpath('//tr[@class=""]')
    #     data = []
    #     for item in items:
    #         topic_id = item.find_element_by_xpath('./td[@class="title"]/a').get_attribute('href')
    #         topic_name = item.find_element_by_xpath('./td[@class="title"]/a').get_attribute('title')
    #         author_id = item.find_element_by_xpath('./td[@nowrap="nowrap"]/a').get_attribute('href')
    #         author_name = item.find_element_by_xpath('./td[@nowrap="nowrap"]/a').text
    #         response_num = item.find_element_by_xpath('./td[@nowrap="nowrap"][2]').text
    #         latest_update = item.find_element_by_xpath('./td[@nowrap="nowrap"][3]').text
    #         data.append((topic_id, topic_name, author_id, author_name, response_num, latest_update))
    #     return data


    @timeConsumed
    def extract_a_page(self):
        soup = BeautifulSoup(self.browser.page_source, "html.parser")
        topics = soup.find('table', class_='olt').find_all('tr', class_='')
        data = []
        for topic in topics:
            zed = topic.find_all('a', class_='')
            rn = topic.find('td', class_='r-count')
            response_num = rn.text if rn else '0'
            latest_response_time = topic.find('td', class_='time').text
            item = (zed[0]['href'],  zed[0]['title'], zed[1]['href'], zed[1].text,
                    response_num, latest_response_time)
            data.append(item)
        return data


    @handleTimeout
    def get_url(self, url):
        self.browser.get(url)
    

    def start(self):
        breakpoint()
        self.get_url(self.url.format(self.start_page*25))
        self.process_pages()