#!/usr/bin/python
# -*- coding: UTF-8 -*-
import logging
import os
import sys
from time import sleep

from bs4 import BeautifulSoup
import requests
import re
from selenium.common.exceptions import NoSuchElementException, TimeoutException,\
                                       ElementClickInterceptedException, ElementNotInteractableException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains

from .MultiPagesCrawler import CrawlerBase
from misc.utils import timeConsumed, logger, handleTimeout, pass_this_saying


class DoubanGallery(CrawlerBase):
    """
    See Also
    ----------
    怎么了解豆瓣上的人，怎么找到有意思的人，

    可以在话题广场上看看有趣的话题，话题下面的广播或者日志就是出自一些参与了这个话题的人, 这些人就不是孤岛了，即使他们关注了零且被零个人关注，他们也和这个世界因为当下所想联系起来了。

    而不是搜ID这个办法到人，这个办法除先后顺序外没别的信息。
    """
    def __init__(self, url, browser, dir_path, file_name):
        super().__init__(url=url, browser=browser, \
                         dir_path=dir_path, file_name=file_name)


    @timeConsumed
    def process_pages(self):
        xpath = '//section[@class="topic-tabs"]/a/span[@class="btn-name"]'
        topics_by_category = self.browser.find_elements_by_xpath(xpath)[1:]
        topics_by_category = [i.text for i in topics_by_category]

        for category in topics_by_category:
            self.browser.find_element_by_partial_link_text(f'{category}').click()
            while True:
                try:
                    we= self.browser.find_element_by_partial_link_text('显示更多')
                    n = len(self.browser.find_elements_by_class_name('topic-link'))
                    logger.info(f'current number of category {category}: {n}')
                    if n > 2:
                        break
                    we.click()
                    sleep(0.1)
                except (ElementClickInterceptedException, ElementNotInteractableException):
                    sleep(0.5)
                except NoSuchElementException:
                    print('xxxx')
                    break
            
            topics_we = self.browser.find_elements_by_xpath('//ul[@class="topic-link-list "]/li[@class="topic-link"]')
            for zed in topics_we:
                topic_title = zed.find_element_by_xpath('./a').text
                gallery_topic_url = zed.find_element_by_xpath('./a').get_attribute('href')
                hot_intensity = zed.find_element_by_xpath('./span').text
                try:
                    preview, articles= re.compile(r'\d+').findall(hot_intensity)
                except ValueError:
                    pass
                topic_related_data = (category, topic_title, gallery_topic_url, preview, articles)
                self.data.append(topic_related_data)             


class DoubanGalleryTopic(CrawlerBase):
    """
    获取豆瓣广场的话题
    """
    def __init__(self, url, browser, dir_path, file_name):
        super().__init__(url=url, browser=browser, \
                         dir_path=dir_path, file_name=file_name)


    def process_pages(self):
        wait = WebDriverWait(self.browser, 10)
        counter = 0
        while True:
            # do the scrolling
            self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            counter += 1
            logger.info('loading new content')
            if counter > 5:
                break
            try:
                wait.until(EC.visibility_of_element_located((By.XPATH, "//*[. = 'Loading...']")))
                logger.info('what is it')
            except TimeoutException:
                sleep(0.1)
                logger.info('try again!')
                # break  # not more posts were loaded - exit the loop
        html = self.browser.execute_script("return document.documentElement.outerHTML")
        soup = BeautifulSoup(html, 'html.parser')
        # 用上vscode里对html/xml 的format工具， 定位元素变得相当容易
        items = soup.find_all('div', class_='topic-item item-status')
        for item in items:
            main_body = item.find('pre', class_="status-full hide")
            text =  main_body.text if main_body  else ''
            tem = item.find('a', class_="author")
            author_name = tem.text
            author_id = tem['href']

            tem = item.find('time', class_='time').find('a')
            ts = tem.text
            link  = tem['href']

            tem = item.find('div', class_='pic-list-wrapper')
            pic_urls = []
            if tem:
                pics = tem.find_all('span', class_="img-wrapper")
                if pics:
                    for pic in pics:
                        pic_urls.append(pic.find('img')['src'])
            values = (author_id, author_name, text, ts, link, pic_urls)

            keys = ('author_id', 'author_name', 'content', 'timestamp', 'link', 'pic_urls')
            info = dict(zip(keys, values))
            self.data.append(info)


class DoubanMovieReviewList(CrawlerBase):
    """
    获取豆瓣电影长评
    """
    def __init__(self, url, browser, dir_path, file_name):
        super().__init__(url=url, browser=browser, \
                         dir_path=dir_path, file_name=file_name)


    def expand_reviews(self):
        while True:
            elements = self.browser.find_elements_by_link_text('展开')
            for element in elements:
                # https://github.com/angular/protractor/issues/4589#issuecomment-344692078
                # WebDriverWait(self.browser, 10).until(EC.element_to_be_clickable((By.LINK_TEXT, '展开'))).click()
                # element.click()
                # sleep(2)
                actions = ActionChains(self.browser)
                actions.move_to_element(element).click().perform()
            try:
                assert not self.browser.find_elements_by_link_text('展开')
                break
            except AssertionError:
                pass


    def process_pages(self):
        '''
        翻页的逻辑都是一样的，在整个代码库已多次重复
        '''

        while True:
            page = self.browser.find_element_by_class_name('thispage').text
            page = int(page)
            logger.info(f'processing page {page}')
            self.extract_a_page()
            if page > 5:
                break
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
            except NoSuchElementException:
                break
             
    
    def extract_a_page(self):
        self.expand_reviews()
        html = self.browser.execute_script("return document.documentElement.outerHTML")
        soup = BeautifulSoup(html, 'html.parser')
        headers = soup.find_all('header', class_="main-hd")
        reviews = soup.find_all('div', class_="main-bd")[1::2]
        cnt = 0
        while len(headers)!=len(reviews):
            logger.info('self.browser.execute_script("return document.documentElement.outerHTML") is not working as expected, try again')
            sleep(2)
            html = self.browser.execute_script("return document.documentElement.outerHTML")
            soup = BeautifulSoup(html, 'html.parser')
            headers = soup.find_all('header', class_="main-hd")
            reviews = soup.find_all('div', class_="main-bd")[1::2]
            cnt += 1
            if cnt > 5:
                break
        if len(headers)!=len(reviews):
            return
        for i in range(len(headers)):
            header = headers[i]
            review = reviews[i]
            tem_1 = header.find('a', class_="name")
            author_id = tem_1['href']
            author_name = tem_1.text
            tem_2 = review.find('div', class_="review-content")
            review_id = tem_2['data-url'] if tem_2 else ''
            review_content = tem_2.text if tem_2 else ''
            values = (author_name,  author_id, review_id, review_content)
            keys = ('author_name', 'author_id', 'review_id', 'review_content')
            info = dict(zip(keys, values))
            self.data.append(info)


class DoubanDoulie(CrawlerBase):
    """
    获取豆列里的信息
    """
    def __init__(self, url, browser, dir_path, file_name):
        super().__init__(url=url, browser=browser, \
                         dir_path=dir_path, file_name=file_name)


    def process_pages(self):
        '''
        连续翻页的逻辑都是一样的，但在代码已出现多次重复
        '''
        previous_page = None
        while True:
            try:
                page = self.browser.find_element_by_class_name('thispage')
            except NoSuchElementException:
                break
            page = int(page.text)
            logger.info(f'processing page {page}')
            self.extract_a_page()
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
            except NoSuchElementException:
                break
    

    def extract_a_page(self):
        items = self.browser.find_elements_by_xpath('//div[@class="doulist-item"]')
        for item in items:
            try:
                tem = item.find_element_by_class_name('title').find_element_by_xpath('./a')
                title = tem.text
                link = tem.get_attribute('href')
                # tem_1 = item.find_element_by_class_name('meta').find_element_by_xpath('./a')
                self.data.append((title, link))
            except NoSuchElementException:
                pass
    

class DoubanBookList(CrawlerBase):
    """
    获取豆瓣图书某个类别的1000本图书
    """
    def __init__(self, url, browser, dir_path, file_name):
        super().__init__(url=url, browser=browser, \
                         dir_path=dir_path, file_name=file_name)


    def process_pages(self):
        '''
        豆瓣只允许访问50页的数据, 即一千本书的条目
        '''
        while True:
            try:
                page = self.browser.find_element_by_class_name('thispage')
            except NoSuchElementException:
                break
            page = int(page.text)
            logger.info(f'processing page {page}')
            html = self.browser.execute_script("return document.documentElement.outerHTML")
            soup = BeautifulSoup(html, 'html.parser')
            data_on_this_page = self.extract_a_page(soup)
            if not data_on_this_page:
                break
            self.data.extend(data_on_this_page)
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
            except NoSuchElementException:
                break
        
    def extract_a_page(self, soup):
        items = soup.find_all('li', class_ = 'subject-item')
        data = []
        for item in items:
            tem = item.find('div', class_='info').find('h2').find('a')
            link = tem['href']
            title = tem['title']
            info = item.find('div', class_='info').find('div', class_='pub').text.strip().split('/')
            info = tuple([i.strip() for i in info])
            info = (link, title) + info
            data.append(info)
        return data


    @handleTimeout
    def get_url(self, url):
        self.browser.get(url)
    

    def start(self):
        self.get_url(self.url.format(0))
        self.process_pages()


# class DoubanBookDetail():
#     raise NotImplementedError('class <DoubanBookDetail> is not implemented')


# class DoubanMovieDetail():
#     raise NotImplementedError('class <DoubanMovieDetail> is not implemented')

# class DoubanReview():
#     raise NotImplementedError('class <DoubanReview> is not implemented')
