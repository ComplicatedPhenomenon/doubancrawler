#!/usr/bin/python
# -*- coding: UTF-8 -*-
from concurrent.futures import ThreadPoolExecutor, as_completed
from datetime import datetime
import json
import os
import re
import sys
from typing import Optional
from time import sleep, time

from bs4 import BeautifulSoup
from pathlib import Path
import requests
from selenium.common.exceptions import NoSuchElementException, TimeoutException,\
                                       ElementNotInteractableException

from .MultiPagesCrawler import MultiPagesCrawler
from misc.utils import timeConsumed, logger, handleTimeout, pass_this_saying


class RegularAction(object):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @handleTimeout
    def get_url(self, url):
        self.browser.get(url)


    def expand_comments(self):
        cnt = 0
        t0 = time()
        while True:
            actions = self.browser.find_elements_by_class_name('give-me-more')
            if actions:
                for action in actions:
                    try:
                        action.click()
                        sleep(1)
                    except ElementNotInteractableException as e:
                        sleep(2)
                        print(e)
                        cnt+=1
                if time() - t0 > 120:
                    print('Stop expanding hidden comments as it takes too long')
                    break
                if cnt >= 3:
                    break
            else:
                break

class DoubanObject(RegularAction):
    '''
    依据url决定引流到哪个具体对象

    Parameters
    -------------
    url : str
        起始url
    browser : selenium.webdriver.chrome.webdriver.WebDriver
        Chrome 驱动

    Attributes
    -------------
    url : str
    browser : selenium.webdriver.chrome.webdriver.WebDriver
    info : dict
        从该url提取的信息, 数据格式{'text': text, 'created_time': created_time, 'comments': comments, \
        'pic_links': pic_links, 'video_links': video_links}

    picture_urls : dict
    video_urls : dict

    Methods
    ----------
    DoubanObject.expand_comments
    DoubanObject.fetch_notes
    DoubanObject.fetch_status


    '''
    def __init__(self, url, browser, **kwargs):
        super().__init__(**kwargs)
        self.url = url
        self.browser = browser
        self.info = None
        self.picture_urls = {}
        self.video_urls = {}


    def _get_soup(self):
        self.expand_comments()
        zed = self.browser.page_source
        soup = BeautifulSoup(zed, 'html.parser')
        return soup


    @timeConsumed
    def fetch_note(self):
        pic_links = []
        video_links = []
        self.get_url(self.url)
        soup = self._get_soup()
        text = soup.find_all('div', class_='note')[1].text
        created_time = soup.find('span', class_='pub-date').text
        pic_links_tem = soup.find_all('div', class_='note')[1].find_all('img')
        if pic_links_tem:
            pic_urls = [i['src'] for i in pic_links_tem]
            pic_links = self._extract_pics_or_videos(created_time, pic_urls, 'jpg')
        comments = self._extract_comments(soup)
        while True:
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
                soup = self._get_soup()
                comments += self._extract_comments(soup)
            except NoSuchElementException:
                break
        self.info = {'text': text, 'created_time': created_time, 'comments': comments, \
                'pic_links': pic_links, 'video_links': video_links}


    @timeConsumed
    def fetch_status(self):
        self.get_url(self.url)
        soup = self._get_soup()
        text = soup.find('blockquote').text
        created_time = soup.find('div', class_='pubtime').text
        pic_urls = soup.find_all('img', class_='upload-pic')
        video_urls = soup.find_all('source', {'type':'video/mp4'})
        pic_links = []
        if pic_urls:
            pic_urls = [i['src'] for i in pic_urls]
            pic_links = self._extract_pics_or_videos(created_time, pic_urls, 'jpg')
        video_links = []
        if video_urls:
            video_urls = [i['src'] for i in video_urls]
            video_links= self._extract_pics_or_videos(created_time, video_urls, 'mp4')
        comments = self._extract_comments(soup)
        while True:
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
                soup = self._get_soup()
                comments += self._extract_comments(soup)
            except NoSuchElementException:
                break
        self.info = {'text': text, 'created_time': created_time, 'comments': comments, \
            'pic_links': pic_links, 'video_links': video_links}


    def _extract_comments(self, soup):
        '''
        * status
        * note
        '''
        res = []
        comments = soup.find('div', class_='comment-list')
        if not comments:
            return res
        comments = comments.find_all('div', class_='item comment-item')
        for comment in comments:
            zed = comment.find('div', class_='comment-item-body')
            meta_info = zed.find('div', class_='meta-header')
            name = meta_info.find('a')['title']
            url  = meta_info.find('a')['href']
            created_time = meta_info.find('time')['datetime']
            text =  zed.find('div', class_='comment-content').text

            reply_list = comment.find('div', class_='reply-list')
            replies = []
            if reply_list != None:
                reply_items = reply_list.find_all('div', class_="item reply-item")
                for reply in reply_items:
                    zed_2 = reply.find('div', class_='comment-main')
                    meta_info_2 = zed_2.find('div', class_='meta-header')
                    name_2 = meta_info_2.find('a')['title']
                    url_2 = meta_info_2.find('a')['href']
                    created_time_2 = meta_info_2.find('time')['datetime']
                    tem = zed_2.find('div', class_='comment-content')
                    try:
                        text_2 = tem.find('span').text
                    except AttributeError:
                        text_2 = '该回应已被删除'
                    reference = tem.find('a', class_='mention-user')
                    refer_to = {} if reference == None else {reference['title']: reference['href']}
                    tem_2 = {'name': name_2, 'url': url_2, 'created_time': created_time_2,
                                'reply_content': text_2, 'refer_to': refer_to}
                    replies.append(tem_2)
            res.append({'name': name, 'url': url, 'created_time': created_time, 'text': text, 'replies': replies})
        return res


    def _extract_pics_or_videos(self, created_time, urls, content_type):
        pattern = re.compile("\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}")
        created_time = pattern.findall(created_time)[0]
        saying_id = created_time
        if content_type == 'jpg':
            self.picture_urls[saying_id] = urls
        elif content_type == 'mp4':
            self.video_urls[saying_id] = urls
        local_links = []
        for item_id, url in enumerate(urls):
            local_links.append(f'{saying_id}_{item_id}.{content_type}')   
        return local_links


class SaveProfileInfo(object):
    """
    获取主页信息
    Parameters
    ------------
    dir_path : str  
        获取信息保存的路径
    file_name : str
        主页名称

    Attributes
    ------------
    SaveProfileInfo.old_info : list
        历史获取的主页信息
    SaveProfileInfo.new_info : list
        新获取的主页信息
    SaveProfileInfo.latest_history : str
        历史信息的最晚时刻
    
    SaveProfileInfo.picture_urls : dict
        主页里的图片 {ID: link, ...}
    SaveProfileInfo.video_urls : dict
        主页里的视频 {ID: link, ...}


    Methods
    --------
    SaveProfileInfo.save : 保存文本信息，下载其中的图片，视频

    """
    def __init__(self, dir_path=None, file_name=None, **kwargs):
        super().__init__(**kwargs)
        self.dir_path = dir_path
        self.file_name = file_name
        self.old_info = []
        self.new_info = []
        self.latest_history = self._check_latest_history()
        self.picture_urls = {}
        self.video_urls = {}


    def _check_latest_history(self):
        history_file = f'{self.dir_path}/{self.file_name}/{self.file_name}.json'
        try:
            with open(history_file, 'r') as f:
                self.old_info = json.load(f)
            latest_history = self.old_info[0]['created_time']
            print(f'only need to get infos late than {latest_history}')
            return latest_history
        # 要么历史文件不存在，要么历史文件为空
        except Exception as e:
            print('No history burden')
            return None


    def _extract_pics_or_videos(self, created_time, urls, content_type):
        pattern = re.compile("\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}")
        created_time = pattern.findall(created_time)[0]
        saying_id = created_time
        if content_type == 'jpg':
            self.picture_urls[saying_id] = urls
        elif content_type == 'mp4':
            self.video_urls[saying_id] = urls
        local_links = []
        for item_id, url in enumerate(urls):
            local_links.append(f'{saying_id}_{item_id}.{content_type}')   
        return local_links


    def save(self, figures=False, videos=False, parallel=False) -> bool:
        Path(f'{self.dir_path}/{self.file_name}').mkdir(parents=True, exist_ok=True)
        if len(self.new_info) != 0: 
            info = self.new_info + self.old_info
            with open(f"{self.dir_path}/{self.file_name}/{self.file_name}.json", 'w') as outfile:
                json.dump(info, outfile, ensure_ascii=False, indent=2)
    
        if not figures and not videos:
            return True

        if parallel:
            if figures:
                self._download_pics_or_videos_parallel('jpg')
            if videos:
                self._download_pics_or_videos_parallel('mp4')
        else: 
            if figures:
                self._download_pics_or_videos('jpg')
            if videos:
                self._download_pics_or_videos('mp4')
        return True


    def _download_pics_or_videos(self, content_type):
        try:
            if content_type == 'jpg':
                urls_dict = self.picture_urls
            elif content_type == 'mp4':
                urls_dict = self.video_urls
            for saying_id, pic_urls in urls_dict.items():
                for pic_id, url in enumerate(pic_urls):
                    self._download_pic_or_video_via_url(url, saying_id, pic_id, content_type) 
            return f'download {content_type} succeessfully'  
        except Exception as e:
            print(e)
            raise ValueError(f'Error when downloading all the pictures: {e}')  
            return f'failed to download {content_type}'


    def _download_pic_or_video_via_url(self, url, saying_id, pic_id, content_type):
        try:
            file_name = f'{self.dir_path}/{self.file_name}/{saying_id}_{pic_id}.{content_type}'
            data = requests.get(url).content
            with open(file_name, 'wb') as handler:
                handler.write(data)
            return f'{saying_id}_{pic_id}.{content_type} download successfully'
        except Exception as e:
            print(e)
            return f'failed to download {saying_id}_{pic_id}.{content_type}'
        

    def _download_pics_or_videos_parallel(self, content_type):
        if content_type == 'jpg':
            urls_dict = self.picture_urls
        elif content_type == 'mp4':
            urls_dict = self.video_urls
        threading = []
        with ThreadPoolExecutor(max_workers=12) as executor:
            for saying_id, pic_urls in urls_dict.items():
                for pic_id, url in enumerate(pic_urls):
                    threading.append(executor.submit(self._download_pic_or_video_via_url,\
                    url, saying_id, pic_id, content_type))     
        for task in as_completed(threading):
            print(task.result())


class DoubanProfileStatus(MultiPagesCrawler, SaveProfileInfo):
    '''
    获取原创的status和note
    '''
    def __init__(self, url, browser, max_pages, dir_path, file_name):
        print('initialize DoubanProfileStatus')
        super().__init__(url=url, browser=browser, max_pages=max_pages, \
                         dir_path=dir_path, file_name=file_name)


    def next_url(self, soup) -> Optional[str]:
        next_span = soup.body.find("span", class_="next")
        if next_span:
            next_link = next_span.find("a")
            if next_link:
                return self.url+next_link['href']
        return None


    def process_page(self, soup) -> bool:
        '''
        提取主页下面的广播信息, 依据关键词 说|写了新日记|转发|想看|看过|想读|读过|听过 筛选
        '''
        pattern = re.compile("说|写了新日记|转发|想看|看过|想读|读过|听过|豆列|收藏小组讨论到豆列|收藏日记到豆列", re.IGNORECASE)
        urls = []
        ts_ls = []
        stop = False
        items = soup.find_all('div', class_='status-wrapper')
        if not items:
            stop = True
        for item in items:
            try:
                actionstr = item.text
            except AttributeError:
                continue
            # 这里可能存在多人接连转发的情况
            actions = re.findall(pattern, actionstr)
            if actions and actions[0] in ['说', '写了新日记', '想看', '看过', '想读', '读过', '收藏小组讨论到豆列', '收藏日记到豆列']:
                print(actions[0])
                breakpoint()
                zed = item.find('span', class_='created_at')
                url = zed.find('a')['href']
                urls.append(url)
                ts = zed['title']
                if pass_this_saying(self.latest_history, ts):
                    stop = True
                    break
                ts_ls.append(ts)
                self.process_a_saying(url)
        return stop


    def process_a_saying(self, url):
        if 'note' in url:
            tem = DoubanObject(url, self.browser)
            tem.fetch_note()
        elif 'status' in url:
            tem = DoubanObject(url, self.browser)
            tem.fetch_status()

        if tem.picture_urls:
            self.picture_urls.update(tem.picture_urls)
        if tem.video_urls:
            self.video_urls.update(tem.video_urls)
        self.new_info.append(tem.info)


class DoubanProfileNote():
    """
    获取主页里的日志
    """
    def __init__(self, url, browser, max_pages, dir_path, file_name):
        self.url = url + '/notes'
        self.browser = browser
        self.max_pages = max_pages
        self.dir_path = dir_path
        self.file_name = file_name
        self.new_info = []


    @timeConsumed
    def process_pages(self):
        notes_url = []
        while True:
            notes = self.browser.find_elements_by_xpath('//div[@class="rr"]/a[@class="j a_unfolder_n"]') 
            if not notes:
                break
            for note in notes:
                notes_url.append(note.get_attribute('href'))
            try:
                self.browser.find_element_by_partial_link_text('后页').click()
            except NoSuchElementException:
                break
        notes_num = len(notes_url)
        for i, url in enumerate(notes_url):
            logger.info(f'total notes {notes_num}, processing note: {i:2}')
            self.get_url(url)
            pub_date = self.browser.find_element_by_xpath('//div/span[@class="pub-date"]').text
            note_body = self.browser.find_element_by_xpath('//div/div[@id="link-report"]/div[@class="note"]').text
            zed = self.browser.page_source
            soup = BeautifulSoup(zed, 'html.parser')
            comments = _extract_comments(soup)
            self.new_info.append({'pub_time': pub_date, 'note': note_body, 'comments': comments})


    def save(self) -> bool:
        Path(f'{self.dir_path}/{self.file_name}').mkdir(parents=True, exist_ok=True)
        if len(self.new_info) != 0:
            info = self.new_info
            with open(f"{self.dir_path}/{self.file_name}/{self.file_name}_note.json", 'w') as outfile:
                json.dump(info, outfile, ensure_ascii=False, indent=2)
    

    def start(self):
        flag = True
        self.get_url(self.url)
        self.process_pages()


    @handleTimeout
    def get_url(self, url):
        self.browser.get(url)


class DoubanProfileSocialNetwork():
    """
    获取以某个人为中心的社交网络
    """
    def __init__(self, url, browser, max_pages):
        self.followings_url = url + '/contacts'
        self.followers_url = url + '/rev_contacts'
        self.browser = browser
        self.max_pages = max_pages
        self.followings = None
        self.followers = None


    @timeConsumed
    def process_pages(self):
        if 'contacts' == self.browser.current_url.split('/')[-1]:
            # 关注页面只有一页，就算有一千以上的 关注人数也只显示在这一页上
            data = []
            zed_ls = self.browser.find_elements_by_class_name('obu')
            n_followings = len(zed_ls)
            for item in zed_ls[:100]:
                people_url = item.find_element_by_xpath('./dd/a').get_attribute('href')
                people_id = people_url.split('/')[-2]
                people_name = item.find_element_by_xpath('./dd/a').text
                data.append((people_name, people_id))
            return data, n_followings

        elif 'rev_contacts' == self.browser.current_url.split('/')[-1]:
            # 被关注的页面每页限制显示为最多七十条
            data = []
            # 首先跳转到最后一页，再往回逐页跳转
            followers = self.browser.find_element_by_xpath('//div[@class="info"]/h1').text
            # find the number between parentheses
            p = re.compile('\((\d+\+?)\)')
            try:
                n_followers = p.findall(followers)[0]
                # bug: 处理豆瓣个人被关注人数为i1000000+的情况
                assert '+' not in n_followers
            except AssertionError:
                n_followers = p.findall(followers)[0]
                return data, n_followers
            try:
                self.browser.find_elements_by_xpath('//div[@class="paginator"]/a')[-1].click()
            except IndexError:
                pass
            while True:
                zed_ls = self.browser.find_elements_by_class_name('obu')  
                for item in zed_ls:
                    people_url = item.find_element_by_xpath('./dd/a').get_attribute('href')
                    people_id = people_url.split('/')[-2]
                    people_name = item.find_element_by_xpath('./dd/a').text
                    data.append((people_name, people_id))
                    if len(data) > 100:
                        return data, n_followers
                try:
                    self.browser.find_element_by_partial_link_text('前页').click()
                except NoSuchElementException:
                    break
            return data, n_followers


    def start(self):
        flag = True
        self.get_url(self.followings_url)
        self.followings = self.process_pages()
        self.get_url(self.followers_url)
        self.followers = self.process_pages()
    

    @handleTimeout
    def get_url(self, url):
        self.browser.get(url)