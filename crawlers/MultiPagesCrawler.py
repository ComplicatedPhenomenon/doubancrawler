#!/usr/bin/python
# -*- coding: UTF-8 -*-
import csv
import datetime
import json
from pathlib import Path
import random
import string
import sys
from typing import Optional

from bs4 import BeautifulSoup
from selenium import webdriver

from misc.utils import timeConsumed, logger, handleTimeout, pass_this_saying


class MultiPagesCrawler(object):
    def __init__(self, url=None, browser=None, max_pages=None, **kwargs):
        super().__init__(**kwargs)
        self.url = url
        self.browser = browser
        self.max_pages = max_pages


    def content(self, a):
        return " ".join(a.contents)


    def fetch(self, url: str):
        try:
            logger.info(f"fetching {url}")
            self.browser.get(url)
            page = {
                'title': self.browser.title,
                'page_source': self.browser.page_source,
            }
            return True,  page
        except Exception:
            return False, None
        

    def last_section(self, url) -> str:
        return url.strip('/').split('/')[-1]


    def next_url(self, soup) -> Optional[str]:
        return None


    def process_page(self, soup) -> bool:
        breakpoint()
        return True


    @staticmethod
    def generate_a_random_string(length=8):
        res= ''.join(random.choice(string.ascii_uppercase+ string.digits )
                     for _ in range(length))
        return res


    def start(self):
        cur_url = self.url
        page_count = 0
        while(cur_url and page_count < self.max_pages):
            logger.info(f"page #{page_count}")
            flag, page = self.fetch(cur_url)
            if not flag:
                break
            soup = BeautifulSoup(page['page_source'], "html.parser")
            stop = self.process_page(soup)
            if stop:
                break
            page_count = page_count + 1
            cur_url = self.next_url(soup)


class CrawlerBase():
    def __init__(self,url=None, browser=None, dir_path=None, file_name=None):
        self.url = url
        self.browser = browser
        self.dir_path = dir_path
        self.file_name = file_name
        self.data=[]


    def save(self, csv_json=True):
        ext = str(datetime.datetime.now().date()).replace('-','')
        Path(f'{self.dir_path}/{self.file_name}').mkdir(parents=True, exist_ok=True)
        file_name = f'{self.dir_path}/{self.file_name}/{self.file_name}_{ext}'
        if csv_json:
            with open(f'{file_name}.csv', 'w', newline='', encoding='utf-8') as f:
                write = csv.writer(f, delimiter=';')
                write.writerows(self.data)
        else:
            with open(f"{file_name}.json", 'w') as f:
                    json.dump(self.data, f, ensure_ascii=False, indent=2)


    @timeConsumed
    def process_pages(self):
        '''refer to: https://stackoverflow.com/a/44575926/7583919
        '''
        raise NotImplementedError           


    @handleTimeout
    def get_url(self, url):
        self.browser.get(url)


    def start(self):
        breakpoint()
        self.get_url(self.url)
        self.process_pages()