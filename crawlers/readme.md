`Douban Movie`
* 可以控制 selenium 让其不是全部加载默认内容，一些样式可以不在页面上加载出来，提高信息提取的效率

* 在展开评论文章时，会出现Exception

    https://www.testim.io/blog/selenium-element-is-not-clickable-at-point/
* 在获取评论文章时，正文除了文字内容，可能还包含图片


获取对象之间的层级关系

-[x] note
-[x] 个人主页
    -[x] status
    -[x] note
-[x] gallery
    -[x] topic
        -[x] status
        -[x] note
-[] movies of a certain type
    -[x] review
    -[] short review
    -[] movie profile
-[x] books of a certain type (一个类型分类下的书)
    -[x] review
    -[] short review
-[x] Doulie
-[x] group
    -[x] topic