----
API
----

.. rubric:: Overview

.. autosummary::
    :nosignatures:


    crawlers.DoubanGroupTopics
    crawlers.DoubanGroupTopics_2
    crawlers.DoubanTopicReply
    crawlers.DoubanTopicReply_2
    crawlers.DoubanProfileStatus
    crawlers.DoubanProfileSocialNetwork
    crawlers.DoubanProfileNote
    crawlers.DoubanObject
    crawlers.SaveProfileInfo
    crawlers.DoubanGallery
    crawlers.DoubanGalleryTopic
    crawlers.DoubanDoulie
    crawlers.DoubanMovieReviewList
    crawlers.DoubanBookList

    filters.cleaner_people_profile
    filters.cleaner_people_profile
    filters.cleaner_topic_reply
    filters.compose_blog
    filters.apply_cleaned_df

    filters.num_vs_time_bar_plot
    filters.date_hour_plot
    filters.plot_num_vs_time_svg
    filters.plot_ps
    filters.bar_plot


信息抓取
=========

DoubanGroupTopics
-------------------
.. autofunction:: crawlers.DoubanGroupTopics

DoubanGroupTopics_2
-------------------
.. autofunction:: crawlers.DoubanGroupTopics_2

DoubanTopicReply
-----------------
.. autoclass:: crawlers.DoubanTopicReply

crawlers.DoubanTopicReply_2
---------------------------
.. autoclass:: crawlers.DoubanTopicReply_2

DoubanProfileStatus
--------------------
.. autofunction:: crawlers.DoubanProfileStatus

DoubanProfileSocialNetwork
----------------------------
.. autofunction:: crawlers.DoubanProfileSocialNetwork

DoubanProfileNote
------------------
.. autofunction:: crawlers.DoubanProfileNote

DoubanObject
-------------
.. autofunction:: crawlers.DoubanObject

SaveProfileInfo
----------------
.. autofunction:: crawlers.SaveProfileInfo

DoubanGallery
--------------
.. autofunction:: crawlers.DoubanGallery

DoubanGalleryTopic
-------------------
.. autofunction:: crawlers.DoubanGalleryTopic

DoubanDoulie
--------------
.. autofunction:: crawlers.DoubanDoulie

DoubanMovieReviewList
----------------------
.. autofunction:: crawlers.DoubanMovieReviewList


DoubanBookList
----------------
.. autofunction:: crawlers.DoubanBookList


原始信息后处理
================

cleaner_people_profile
----------------------
.. autofunction:: filters.cleaner_people_profile


cleaner_topic_reply
-------------------
.. autofunction:: filters.cleaner_topic_reply


compose_blog
-------------
.. autofunction:: filters.compose_blog


clean_group_topic
------------------
.. autofunction:: filters.clean_group_topic

apply_cleaned_df
----------------
.. autofunction:: filters.apply_cleaned_df


画图函数
========

num_vs_time_bar_plot
--------------------
.. autofunction:: filters.num_vs_time_bar_plot

date_hour_plot
---------------
.. autofunction:: filters.date_hour_plot

date_hour_plot
---------------
.. autofunction:: filters.plot_num_vs_time_svg

bar_plot
----------
.. autofunction:: filters.bar_plot

plot_ps
----------- 
.. autofunction:: filters.plot_ps