.. Douban Crawler documentation master file, created by
   sphinx-quickstart on Sat Dec 18 09:03:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Douban Crawler's documentation!
==========================================
豆瓣爬虫获取多个页面的信息，包括小组，小组的具体话题，个人主页，豆列里的收藏， 话题广场下的所有类别下的所有话题， 一个话题下的所有内容,
一部电影的长评, 个类别下的1000本书籍, 一篇日志

处理获取的信息 


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   api
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`