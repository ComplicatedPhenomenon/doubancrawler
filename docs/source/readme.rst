-------
README
-------

主入口
=======
此库提供主程序``test_new_features.py``, 另外有一些功能不提供公开调用，鼓励进行手动尝试。

.. code:: bash

    python3 test_new_features.py --help


抓取对象
========
#. group
    #. topic
#. 个人主页
    #. status
    #. note
#. Doulie
#. gallery
    #. topic
        #. status
        #. note
#. movies of a certain type
    #. review
    #. short review (待完成)
    #. movie profile (待完成)
#. books of a certain type (一个类型分类下的书)
    #. review (待完成)
    #. short review (待完成)


豆瓣的限制
==========

被豆瓣的限制历程

1. 在一次多线程高速请求之后，24小时内网络不可访问它的服务。

    
    
2. 提示IP异常 selenium 要求登录

    .. image:: ../image/1.png
       :scale: 35 %

3. 密码登录被限制

    .. image:: ../image/2.png
       :scale: 35 %

4. 验证码登录被限制

    .. image:: ../image/3.png
       :scale: 35 %


如果需要获取大量数据， 多个IP, 多个真实账号来比较稳妥，少量数据获取没问题。



