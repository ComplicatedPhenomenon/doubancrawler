import pandas as pd
from mlxtend.frequent_patterns import apriori
from mlxtend.preprocessing import TransactionEncoder
from collections import defaultdict, deque


def get_index(string, li):
    result = []
    for i, value in enumerate(li):
        if value == string:
            result.append(i)
    return result


def getapr1(normal_dataset, minsup):
    te = TransactionEncoder()
    te_ary = te.fit(normal_dataset).transform(normal_dataset)
    df = pd.DataFrame(te_ary, columns=te.columns_)
    df = apriori(df, min_support=minsup, use_colnames=True).sort_values(by = 'support', ascending = False)
    df['length'] = df['itemsets'].apply(lambda x: len(x))
    hf1 = set([list(i)[0] for i in df.loc[df['length'] == 1]['itemsets'].values])
    return hf1


def high_freq_seg(ll, minsup):
    hf1 = getapr1(ll, minsup)
    len_data = len(ll)
    satis_dic = {}
    tasks = deque()
    
    for i in hf1:
        task = (i,)
        tasks.append(task)
        satis_dic[task] = {}
        for j in range(len_data):
            indexes = get_index(i, ll[j])
            if len(indexes) != 0:
                satis_dic[task][j] = indexes

    while tasks:
        task = tasks.popleft()
        new_tasks = []
        for num, indexes in satis_dic[task].items():
            for index in indexes:
                next_index = index + 1
                if next_index == len(ll[num]):
                    continue
                next_item = ll[num][next_index]
                if next_item not in hf1:
                    continue
                new_task = tuple(list(task) + [next_item])
                if new_task not in satis_dic:
                    satis_dic[new_task] = defaultdict(list)
                    new_tasks.append(new_task)
                satis_dic[new_task][num].append(next_index)
        for nt in new_tasks:
            if len(satis_dic[nt].keys()) < len_data * minsup:
                del satis_dic[nt]
            else:
                tasks.append(nt)
        satis_dic[task] = len(satis_dic[task])
    
    itemsets = list(satis_dic.keys())
    num = list(satis_dic.values())
    support = [i / len_data for i in satis_dic.values()]

    re = pd.DataFrame({'itemsets': itemsets, 'support': support})
    re['length'] = re['itemsets'].apply(lambda x: len(x))
    re['number'] = num
    re = re.sort_values(by="support" , ascending=False)
    return re


def get_hfs_result(hfs, ll2):
    numintable = []
    for i in range(len(hfs)):
        item = hfs.iloc[i,0]
        cnt = 0
        for j, l2 in enumerate(ll2):
            if check_subinlist(item, l2):
                cnt+=1
        numintable.append(cnt)
    hfs['number_in_normal'] = numintable
    hfs['tp/fp'] = round(hfs['number'] / hfs['number_in_normal'],3)
    return hfs.sort_values(by='tp/fp', ascending=False)



def check_subinlist(substring, li):
    substring = list(substring)
    li = list(li)
    head = substring[0]
    n = len(substring)
    head_index = get_index(head,li)

    if head_index != []:
        for i in head_index:
            if li[i:(i+n)] == substring:
                return True
    return False

