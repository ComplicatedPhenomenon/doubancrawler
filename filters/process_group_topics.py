import datetime
import re
from os import listdir
from glob import glob
from pathlib import Path

import requests
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from matplotlib.font_manager import FontProperties
import numpy as np

from filters.visualize_data import bar_plot, plot_ps

from misc.constants import FONT_PATH
myfont = FontProperties(fname=FONT_PATH, size=15)


class ZuFang():
    def __init__(self, group, places):
        self.path = f'data/crawler/douban_group_topics/{group}'
        self.places = places
        self.path2 = f'data/filter/douban_group_topics/figs/{group}'
        Path(self.path2).mkdir(parents=True, exist_ok=True)
        self.df = self._merge_data()


    def plot_ps(self, ps: pd.Series, file_name: str, use_with_streamlit:bool = False):
        fig, ax = plt.subplots(1)
        fig.set_size_inches(w=24, h=8)
        ax.plot(ps.index, ps.values, marker='.')
        for label in ax.xaxis.get_majorticklabels():
            label.set_fontproperties(myfont)
        if not use_with_streamlit:
            plt.savefig(f'{self.path2}/{file_name}.svg', dpi=120, format='svg')
            plt.close()
        else:
            return fig


    def compose_num_vs_place(self):
        df = self.df[self.df.pt.str.contains('|'.join(self.places))]
        data = []
        for place in self.places:
            tem = df[df.pt.str.contains(place)].shape[0]
            data.append(tem)
        ps = pd.Series(data)
        ps.index = self.places
        return ps


    def _merge_data(self):
        files = listdir(self.path)
        data = []
        for file in files:
            x = re.compile(r'\d{8}').findall(file)
            if x:
                df = pd.read_csv(f'{self.path}/{file}', sep=';')
                data.append(df)
        df = pd.concat(data, axis=0)
        new_df = df.drop_duplicates(subset=df.columns[1:-2])
        new_df = df.drop_duplicates(subset=df.columns[1:-2]).copy()
        # loss = new_df.shape[0]/df.shape[0]
        # Make sure ts isn't illegal
        # https://www.dataquest.io/blog/settingwithcopywarning/
        new_df.loc[:, 'ts'] = new_df.ts.copy().map(lambda item: '2021-'+item)
        new_df.ts = pd.to_datetime(new_df.ts)
        return new_df


    def analyse(self):
        """
        获取小组统计信息并画图
        """
        ps = self.df.groupby(pd.Grouper(key='ts', freq='H')).size()
        self.plot_ps(ps, 'hour')
        ps = self.compose_num_vs_place()
        self.plot_ps(ps, 'hot_spot')
        self._get_reliable_resource()


    def _get_structured_info_from_topic_title(self, tt):
        '''
        https://docs.python.org/3/library/re.html
        从小组话题的标题中提取结构化的信息
        '''
        roomtype = re.search('主卧|次卧', tt)
        roomtype = roomtype.group(0) if roomtype else ''

        tel = re.search('\d{11}', tt)
        if tel:
            tel = tel.group(0)
            tt = tt.replace(tel, '')
        else:
            tel = ''

        cost = re.search('\d{4}', tt)
        if cost and len(re.findall('\d{4}', tt)) == 1:
            cost = cost.group(0)
            tt = tt.replace(cost, '')
        else:
            cost = ''

        area = re.search(r'(\d+)平|平米', tt)
        area = area.group(0) if area else ''
        washingroom = re.search('独卫|独立卫生间', tt)
        washingroom = 'Y' if washingroom else ''
        rent_type = re.search('直租|转租|求租', tt)
        if rent_type:
            rent_type = rent_type.group(0)
        else:
            rent_type = ''
        return (tel, cost, roomtype, area, washingroom, rent_type)


    def _get_reliable_resource(self):
        """
        筛选出小组里发帖比较少的发布者发布的帖子
        """
        ps = self.df.groupby('al').size().sort_values(ascending=False)
        self.df.loc[:, 'sinfo'] = self.df.pt.map(self._get_structured_info_from_topic_title)
        zj = ps[ps>3].index
        zed = self.df[~self.df.al.isin(zj)].copy()

        zed = zed.sort_values('ts', ascending=False)
        ts0 = zed.ts.max() - datetime.timedelta(days=4)
        zed = zed[zed.ts > ts0]
        zed_1 = zed.apply(lambda item: len(re.findall('|'.join(self.places), item['pt']))!=0 \
                                    and (item['sinfo'][1] == '' or '2000' < item['sinfo'][1] <= '3000'), axis=1)
        zed[zed_1][['pt','pl']].to_csv(f'{self.path}/reliable_resource.csv', sep=';', index=False)


def clean_group_topic(group):
    """
    清洗小组的数据， 多个新获取的文件合并去重，规范不完整的数据，最后与之前清洗过的数据合并。

    Parameters
    ----------
    group : str
        小组的名称

    Returns
    -------
    df : pd.DataFrame

    """

    path = 'data/crawler/douban_group_topics/'
    files = glob(path+f'/{group}/{group}*.csv').copy()
    new_data = []
    old_df = pd.DataFrame()
    for fs in files:
        if re.findall(r'edited', fs):
            old_df = pd.read_csv(fs, sep=';')
        else:
            with open(fs, 'r') as f:
                new_data.extend(f.readlines())
    data2 = []
    for j, row in enumerate(new_data):
        try:
            assert len(row.split(';'))==6
            data2.append(row)
        except AssertionError:
            pass
    new_df = pd.DataFrame()
    if data2:
        new_df = pd.DataFrame(data2).drop_duplicates()
        new_df = pd.DataFrame(new_df[0].str.split(';').tolist(), columns=['pl', 'pt', 'al','an', 'cnt', 'ts'])
        new_df.ts = new_df.ts.str.strip('\n')
        new_df = new_df.drop_duplicates(subset=new_df.columns[1:-2])
        new_df.cnt = new_df.cnt.str.strip().replace('', '0').astype(int)
        new_df.loc[new_df.ts.str.findall('\d{2}-\d{2} \d{2}:\d{2}').map(len)!=0, 'ts'] = '2021-' + \
            new_df.loc[new_df.ts.str.findall('\d{2}-\d{2} \d{2}:\d{2}').map(len)!=0, 'ts']

    if old_df.shape[0]!=0:
        df = pd.concat([old_df, new_df], axis=0).drop_duplicates(subset=old_df.columns[1:-2], ignore_index=False)
    else:
        df = new_df

    if df.shape[0] > old_df.shape[0]:
        for fs in files:
            print(f'{fs} 🌶')
            Path(fs).unlink()
    elif df.shape[0] == old_df.shape[0]:
        ext = str(datetime.datetime.now().date()).replace('-','')
        df.to_csv(f'{path}/{group}/{group}_edited_{ext}.csv', sep=';', index=False)
    else:
        breakpoint()
    return df


def apply_cleaned_df(df, group):
    """
    对清洗后的小组数据，获取统计信息

    Parameters
    ----------
    df : pd.DataFrame
         clean_group_topic 产生的数据
    group : str
        小组的名称

    Returns
    -------
    filename_url_dict : dict
        这个小组下的话题 {名称: 链接}
    """
    path = 'data/crawler/douban_group_topics/'
    df.ts = pd.to_datetime(df.ts)
    df = df.sort_values('ts', ascending=False)
    extension = str(datetime.datetime.now().date()).replace('-','')
    df.to_csv(f'{path}/lessky/lessky_edited_{extension}.csv', index=False)

    filename_url_dict = _fetch_profile_topic(df)
    lessky(df)
    ps = df.groupby(pd.Grouper(key='ts', freq='D')).size()
    dfx = ps.reset_index().rename({0: 'value', 'ts': 'date'}, axis=1)
    paths =[f'data/filter/douban_group_topics/csv/{group}', f'data/filter/douban_group_topics/figs/{group}']
    for path in paths:
        Path(path).mkdir(parents=True, exist_ok=True)
    dfx.to_csv(f'{paths[0]}/{group}_date_vs_num.csv', index=False)
    plot_ps(ps, f'{paths[1]}', 'num_vs_date', tick={'dtfmt': '%Y-%m-%d', 'locator': 'D', 'interval': 15})
    abnormal = ps.sort_values(ascending=False)
    ps = df[df.ts > pd.to_datetime('2021-01-01')].groupby(pd.Grouper(key='ts', freq='H')).size()
    plot_ps(ps, f'{paths[1]}', '2021_num_vs_H', tick={'dtfmt':'%m-%d %H', 'locator': 'H', 'interval': 12})
    return filename_url_dict


def lessky(df):
    """
    针对具体小组，定制化信息提取
    """

    cnt = df.groupby('al').ngroups
    cnt_0 = df[df.an.str.contains('已注销')].groupby('al').ngroups
    zed = df.groupby('al').size().sort_values(ascending=False)
    n_topics = zed.iloc[:1000].sum()
    kxr_lz = zed[zed > 5].index.tolist()


    ps = df.groupby('al')['ts'].agg(np.ptp).sort_values(ascending=False)
    lyt_lz = ps[ps > pd.Timedelta(days=1000)].index.tolist()

    ps = (ps.dt.days).value_counts().sort_index()
    title = 'number of people vs existed days'
    prefix = '_'.join(title.split(' '))
    path = 'data/filter/douban_group_topics/figs/lessky/'
    Path(path).mkdir(parents=True, exist_ok=True)
    plot_ps(ps, f'{path}/', f'{prefix}_line', scale={'x':'log', 'y': 'log'})
    split_position_ls = [-1, 0, 1, 5, 30, 90]+[365*i for i in range(1, 12)]
    ps = _compose_ps(ps, split_position_ls)
    bar_plot(ps, title, f'{path}/', f'{prefix}_bar')

    ps = df[df.cnt>3000].groupby('al').size()
    hot_lz = ps.index.tolist()

    ps = df[df.cnt==0].groupby('al').size().sort_values(ascending=False)
    miserable_lz = ps[ps>5].index.tolist()

    ps = df.groupby('al').size().value_counts().sort_index()
    title = 'number of people vs number of posts'
    prefix = '_'.join(title.split(' '))
    plot_ps(ps, f'{path}/', f'{prefix}_line', scale={'x':'log', 'y': 'log'})
    split_position_ls = [0, 1, 5, 10, 30, 50, 100, 200, 300, 400, 500, 1000]
    ps = _compose_ps(ps, split_position_ls)
    bar_plot(ps, title, f'{path}/', f'{prefix}_bar')

    ps = df.cnt.value_counts().sort_index()
    title = 'posts number vs response number'
    prefix = '_'.join(title.split(' '))
    plot_ps(ps, f'{path}/', f'{prefix}_line', scale={'x':'log', 'y': 'log'})
    split_position_ls = [-1, 0, 10, 50, 100, 500, 1000, 3000, 10000, 30000, 100000]
    ps = _compose_ps(ps, split_position_ls)
    bar_plot(ps, title, f'{path}/', f'{prefix}_bar')


def _compose_ps(ps, split_position_ls):
    '''
    split_position_ls is hard coded here, this process still determined by personal experience,
    yet the logic behind this hasn't been extracted out
    '''
    intervals = pd.cut(ps.index, split_position_ls)
    zed = ps.tolist()
    ps_2 = ps.groupby(intervals).count()
    ri = ps_2.cumsum().tolist()
    le = ps_2.cumsum().shift(1).fillna(0).astype(int).tolist()
    values = [sum(zed[le[i]:ri[i]+1])  for i in range(ps_2.shape[0])]
    index = [f'({i.left}, {i.right}]' for i in ps_2.index]
    new_ps = pd.Series(values, index)
    return new_ps


def _fetch_profile_topic(df):
    for col in ['pt', 'an']:
        df.loc[:, col] = df[col].map(_exclude_punctuation)
    filename_url_dict = df.sort_values('cnt', ascending=False).iloc[:100][['pt', 'pl']].values.tolist()
    filename_url_dict = dict(filename_url_dict)
    return filename_url_dict


def _exclude_punctuation(string):
    #生成douban_group_topic 和 douban_people_profile 的文件名和链接
    try:
        pattern = '([\d\w[\u4e00-\u9fff]+]*?)'
        res = re.findall(pattern, string)
    except TypeError:
        res = ['hahaha']
    return '_'.join(res)


def test_gaode_api():
    """
    试验高德地图的api
    """
    path = 'data/crawler/douban_group_topics/'
    xls=pd.ExcelFile(f'{path}/amap_poicode.xlsx')
    df = pd.read_excel(xls, xls.sheet_names[2])

    url_b = 'https://restapi.amap.com/v3/place/text?'
    key = '7a6cda8370bc63128db8191b5298d349'
    city = '110000'
    types = '150500'
    page_index = 1

    ext = f'&citylimit=true&children=1&offset=20&page={page_index}&extensions=all'
    url = url_b + f'key={key}&types={types}&city={city}{ext}'
    response = requests.get(url)
    cnt = response.json()['count']

    url = url = 'https://restapi.amap.com/v3/direction/driving?origin=116.45925,39.910031&destination=116.587922,40.081577&output=xml&key=7a6cda8370bc63128db8191b5298d349'
    response = requests.get(url)
    response.text
    breakpoint()

    df_ls = []
    for page_index in range(2, int(cnt)//20+2):
        ext = f'&citylimit=true&children=1&offset=20&page={page_index}&extensions=all'
        url = url_b + f'key={key}&types={types}&city={city}{ext}'
        response = requests.get(url)
        df = pd.DataFrame(response.json()['pois'])
        try:
            print(page_index)
            print(df.loc[:,['address', 'adname', 'business_area', 'name']])
        except KeyError:
            breakpoint()
        df_ls.append(df)

    df = pd.concat(df_ls, axis=0).loc[:,['address', 'adname', 'business_area', 'name']]