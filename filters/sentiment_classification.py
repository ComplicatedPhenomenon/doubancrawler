from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor, as_completed
from pprint import pprint 
import time 
import configparser

import requests
from requests.exceptions import ConnectionError, ConnectTimeout, ReadTimeout

from misc.utils import timeConsumed, logger


def test_api(url, method, parameters, text_id=None):
    flag = True
    while flag:
        try:
            karg = {('params' if method=='get' else 'json'): parameters}
            response = getattr(requests, method)(url, **karg)
            if response.status_code == 200 :
                resp = response.json()
                if not text_id and text_id!=0:
                    return resp
                elif text_id==0 or text_id:
                    if 'items' in  resp:
                        print(f'response for {text_id:4} is: {response.status_code}')
                        return (text_id, resp['items'][0])
                    else:
                        print(f'even status code is 200, yet baidu does not give you proper data, try again...')
        except (ConnectTimeout, ReadTimeout, ConnectionError) as e:
                print(f'{e}, try again ...')


def multi_threading_fetch(a_func, url, method, parameters):
    futures = []
    with ThreadPoolExecutor(128) as executor:
        for text_id, param in enumerate(parameters):
            futures.append(executor.submit(a_func, url, method, param, text_id))
    res = []
    for task in as_completed(futures):
        res.extend(task.result())
    return res


def get_access_token():
    '''
    获取Access Token
    https://ai.baidu.com/ai-doc/REFERENCE/Ck3dwjhhu
    '''
    config = configparser.ConfigParser()
    config.read('config.ini')
    client_id = config['baidu']['client_id']
    client_secret = config['baidu']['client_secret']
    url = 'https://aip.baidubce.com/oauth/2.0/token'
    params = {'grant_type': 'client_credentials', 
              'client_id': client_id, 
              'client_secret': client_secret}
    # test_api(url, 'post', params) failed
    resp = test_api(url, 'get', params)
    assert 'access_token' in resp
    return resp['access_token']


def main():
    AT = get_access_token()
    url = 'https://aip.baidubce.com/rpc/2.0/nlp/v1/sentiment_classify?charset=UTF-8&access_token={}'
    params={"text": "苹果是一家伟大的公司"}
    # text	string	文本内容，最大2048字节， 即最多1024个汉字
    resp = test_api(url.format(AT), 'post', params)
    pprint(resp)
    url = 'https://aip.baidubce.com/rpc/2.0/nlp/v2/comment_tag?charset=UTF-8&access_token={}'
    params = {"text":"三星电脑电池不给力", "type":13}
    resp = test_api(url.format(AT), 'post', params)
    pprint(resp)


if __name__ == '__main__':
    main()
    