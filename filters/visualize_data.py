import datetime
from random import sample
import re
import time

# import jieba
# import jieba.posseg as pseg
import matplotlib.pyplot as plt
# https://matplotlib.org/3.1.1/gallery/text_labels_and_annotations/date.html
import matplotlib.dates as mdates
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from matplotlib import rcParams
from matplotlib.font_manager import FontProperties
import numpy as np
from pylab import MaxNLocator
import pandas as pd
from PIL import Image
from wordcloud import WordCloud, STOPWORDS

from misc.constants import FONT_PATH
from misc.utils import timeConsumed, logger

rcParams['axes.unicode_minus'] = False
# jieba.enable_paddle()

def read_stopwords():
    try:
        with open('./filters/stopwords_cn.txt', 'r') as f:
            data = f.readlines()
        data = [i.strip('\n') for i in data]
    except FileNotFoundError:
        data = []
    return data

STOPWORDS.update(set(read_stopwords()))
myfont = FontProperties(fname=FONT_PATH, size=15)


def plot_num_vs_time_svg(df, path):
    '''
    绘制描述不同时间段聚合的评论数量的折线图

    Parameters
    --------------------
    df : pd.DataFrame 
        df.column: participants names
        df.index: single level index or MultiIndex with two levels
    '''
    fig, ax = plt.subplots()
    fig.set_size_inches(w=24, h=8)
    
    x = df.index
    if df.index.nlevels == 2:
        # 为数据量身定制的尺寸
        w=df.shape[0]*0.1
        fig.set_size_inches(w=w, h= 8 if w/45 < 8 else w/45 )
        x = pd.to_datetime([f'{i[0]} {i[1]:02}' for i in x])
    for name in df.columns:
        zed_ps = df.loc[:, name]
        ax.plot(x, zed_ps.values, marker='.', label=name)
    
    ax.set_xlim(xmin=x[0], xmax=x[-1])
    if df.index.nlevels == 2:
        myFmt = mdates.DateFormatter('%m-%d %H')  
        ax.xaxis.set_major_formatter(myFmt)
        ax.xaxis.set_major_locator(mdates.HourLocator(interval=12))
    
    ax.legend(prop=myfont)
    fig.autofmt_xdate()
    fig.tight_layout()
    fn = 'hour' if set(df.index)== set(range(24)) else ('date' if df.index.nlevels == 1 else 'date_hour')
    plt.savefig(f'{path}/{fn}.svg', dpi=120, format='svg')
    plt.close()


def num_vs_time_bar_plot(data):
    """
    绘制评论数量与时段的折线图，支持
    1. 按小时聚合的数量
    2. 按日期聚合的数量

    Parameters
    -------------
    data: tuple
        it has 3 elements (ps, file, ext)
    """
    ps, file_name, ext = data
    print(f'plot {file_name}_{ext}')
    b = ps.values.min()
    fig, ax = plt.subplots(1)
    fig.set_size_inches(0.5*ps.shape[0], 5)
    if ext=='hour':
        # https://stackoverflow.com/a/55237837/7583919
        cols = ['lightcoral' if (1< x < 5) else 'bisque' for x in ps.index]
        ax.bar(x=ps.index, height=ps.values, width=1, bottom=b, edgecolor='r', color=cols)
        ax.set_xlabel('hour')
    elif ext=='date':
        # https://www.earthdatascience.org/courses/use-data-open-source-python/use-time-series-data-in-python/date-time-types-in-pandas-python/customize-dates-matplotlib-plots-python/
        # https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        ax.bar(x=ps.index, height=ps.values, width=1, bottom=b, edgecolor='r', color='bisque')
        ax.set_xlabel('date')
        myFmt = mdates.DateFormatter('%b-%d')
        ax.xaxis.set_major_formatter(myFmt)
        ax.xaxis.set_major_locator(mdates.WeekdayLocator(interval=1))
    fig.tight_layout()
    plt.savefig(f'{file_name}_{ext}.png', dpi=120)
    plt.close()


def date_hour_plot(item, file):
    '''
    绘制多个子图，每个子图描述一个日期的情况, 每个子图是一个柱状图，描述具体日期上每个时段聚合的评论数量
    
    Parameters
    -------------
    item: List 
        元组构成的列表，一个元组均含两个元素 [(date, ps), (date, ps), ...] ps.index time ps.values num
    '''
    assert len(item) != 0
    num_fig = len(item)
    # nrows=num_fig//3+1
    v1, v2 = divmod(num_fig, 3)
    nrows = v1+1 if v2 else v1
    
    fig, ax = plt.subplots(nrows=nrows, ncols=3, sharex=False, sharey=False)
    ax = np.atleast_2d(ax)
    assert ax.shape == (nrows, 3)
    fig.set_size_inches(w=1.5*item[0][1].shape[0], h=4*nrows)

    fig.subplots_adjust(hspace=0.6)

    pattern = r'\d{4}-\d{2}-\d{2}'
    flag = re.findall(pattern, item[0][0])
    case = 'name' if not flag else 'date'
    for i, v in enumerate(item):
        p1, p2 = divmod(i, 3)
        title, ps = v
        b = ps.values.min()
        if case == 'date':
            cols = ['lightcoral' if (1 < x < 5) else 'bisque' for x in ps.index]
            ax[p1, p2].set_xlabel('hour')
        elif case == 'name':
            title_info = title.split(',')
            title = title_info[0]
            ylabel = title_info[1]
            cols = ['bisque' for x in ps.index]
            ax[p1, p2].set_xlabel('name')
            ax[p1, p2].set_ylabel(f'{ylabel}')
            ax[p1, p2].set_xticklabels(labels = ps.index, rotation='vertical')

        ax[p1, p2].bar(x=ps.index, height=ps.values, width=1, bottom=b, edgecolor='b', color=cols)
        if case == 'name':
            for label in ax[p1, p2].xaxis.get_majorticklabels():
                label.set_fontproperties(myfont)
                label.set_fontsize(8)
                label.set_rotation(45)
    
        ax[p1, p2].set_title(f'{title}', fontproperties=myfont)
        # https://stackoverflow.com/q/12050393/7583919 force y axis to only use integers
        ya = ax[p1, p2].get_yaxis()
        ya.set_major_locator(MaxNLocator(integer=True))

    try:
        fig.tight_layout()
        plt.savefig(f'{file}_bar.svg', format = 'svg')
        logger.info(f'bar plot nums vs date hour for {file}')
        plt.close()
    except ValueError:
        logger.info(f'bar plot Image {file} size is too large, stop plotting')
        pass

    if case == 'date':
        date_hour_ps = []
        for date, ps in item:
            ps.index = pd.to_datetime([f'{date} {i:02}' for i in ps.index])
            date_hour_ps.append(ps)
        date_hour_ps = pd.concat(date_hour_ps)
        fig, ax = plt.subplots()
        fig.set_size_inches(3*ps.shape[0], 4)
        x = date_hour_ps.index
        y = date_hour_ps.values
        ax.plot(x, y)
        myFmt = mdates.DateFormatter('%m-%d %H')
        ax.set_xlim(xmin=x[0], xmax=x[-1])
        ax.xaxis.set_major_formatter(myFmt)
        ax.xaxis.set_major_locator(mdates.HourLocator(interval=12))
        fig.tight_layout()
        fig.autofmt_xdate()
        plt.savefig(f'{file}_line.png', format='png', dpi=90)
        logger.info(f'line plot nums vs date for {file}')
        plt.close()


def plot_wordscloud(content, file):
    '''
    自动判断高频词汇的词性，去除不应该出现在词云上的词汇
    '''
    words_list = jieba.cut(content, cut_all=False)
    words= ' '.join(words_list)
    # 根据词性再挑选出一部分 stopwords
    n_words = len(content)
    word_limit = 50000
    difang=set()
    shijian= set()
    stopwords=set()

    # long content cost large resource, cut it into small pieces
    contents = [content[i: i+word_limit] for i in range(0, n_words, word_limit)] 
    for content in contents:
        words_2 = pseg.cut(f'{content}', use_paddle=True)
        # m数量词 q量词 r代词 p介词
        # c连词	u助词 xc其他虚词 w标点符号
        for word, flag in words_2:
            if flag in ['m', 'q', 'r', 'p', 'c', 'u', 'xc', 'w']:
                stopwords.add(word)
            elif flag in ['ORG']:
                difang.add(word)
            elif flag in ['t', 'TIME']:
                shijian.add(word)
    STOPWORDS.update(stopwords)
    # if difang:
    #     logger.info(f'{difang}')
    # if shijian:
    #     logger.info(f'{shijian}')
    wc = WordCloud(background_color="white",
                    width=800,
                    height=600,
                    collocations=False,
                    stopwords=STOPWORDS,
                    font_path=FONT_PATH)
    wc.generate_from_text(words)
    # wc.words_ 可以看出每个词语将在词云图里面占得的尺寸相对比例
    # https://zhuanlan.zhihu.com/p/58855751
    wc.to_file(f'{file}.png')
    logger.info(f'complete {file}')


def bar_plot(ps, title, path, file_name):
    """
    小组统计信息的画图函数
    """
    fig, ax = plt.subplots(1)
    ax.bar(ps.index, ps.values, width=0.8, edgecolor='black', color='red')
    ax.set_xticklabels(labels=ps.index, rotation=45)
    ax.set_title(title)
    ax.set_ylabel('Count')
    ax.set_xlabel('Intervals')
    ax.set_yscale('log')
    xlocs, _ = plt.xticks()
    for i, val in enumerate(ps.values):
        plt.text(xlocs[i], val+0.01, str(val), horizontalalignment="center")
    fig.tight_layout()
    plt.savefig(f'{path}/{file_name}.svg', dpi=120, format='svg')
    plt.close()


def plot_ps(ps, path, file_name, tick=None, scale={'x':'linear', 'y':'linear'}):
    """
    小组统计信息的画图函数
    """
    x = ps.shape[0]
    fig, ax = plt.subplots(1)
    fig.set_size_inches(w=x//20, h=8)
    ax.plot(ps.index, ps.values, marker='.')

    # 左右两边均有刻度
    ax.tick_params(labelright=True)
    if tick:
        ax.set_ylim(bottom=0, top=ps.max())
        ax.set_xlim(left=ps.index.min(), right=ps.index.max())
        myFmt = mdates.DateFormatter(tick['dtfmt'])  
        ax.xaxis.set_major_formatter(myFmt)
        if tick['locator'] == 'D':
            ax.xaxis.set_major_locator(mdates.DayLocator(interval=tick['interval']))
        elif tick['locator'] == 'H':
            ax.xaxis.set_major_locator(mdates.HourLocator(interval=tick['interval']))
            ax.grid(color='azure', linestyle='-', linewidth=1)
        fig.autofmt_xdate()
    else:
        ax.set_xscale(scale['x'])
        ax.set_yscale(scale['y'])
    plt.savefig(f'{path}/{file_name}.svg', dpi=120, format='svg')
    plt.close()