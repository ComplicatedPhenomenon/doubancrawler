import sys

LOGGER_FORMAT = '[%(asctime)s] %(message)s'
CRAWLER_FOLDER = "data/crawler/"
FILTER_FOLDER = "data/filter/"
CHROME_DRIVE_PATH = "./bin/chromedriver"

if sys.platform == 'linux':
    CHROME_DRIVE_PATH = './bin/chromedriver_linux'
    USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'
    FONT_PATH = '/usr/share/fonts/opentype/noto/NotoSansCJK-Regular.ttc'
elif sys.platform == 'darwin':
    CHROME_DRIVE_PATH = './bin/chromedriver_mac'
    USER_AGENT = 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36'
    FONT_PATH = '/Library/Fonts/Songti.ttc'
class CRAWLER_TYPE:
    DOUBAN_GROUP_LIST =  'douban_group_list'
    DOUBAN_DISCUSSION_LIST = 'douban_discussion_list'
    DOUBAN_GROUP_TOPICS =  'douban_group_topics'
    DOUBAN_TOPIC_REPLY = 'douban_group_topic'
    DOUBAN_PEOPLE_PROFILE = 'douban_people_profile'
    DOUBAN_NOTE = "douban_note"
    DOUBAN_DOULIE = 'douban_doulie'
    DOUBAN_MOVIE_REVIEW_LIST = 'douban_movie_review_list'
    DOUBAN_GALLERY_TOPICS='douban_gallery_topics'
    DOUBAN_SOCIAL_NETWORK = 'douban_social_network'
    DOUBAN_BOOK_LIST = 'douban_book_list'