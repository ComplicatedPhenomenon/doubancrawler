import argparse
from pathlib import Path
import pickle

import pandas as pd
from selenium import webdriver

from crawlers import DoubanMovieReviewList, DoubanBookList
from crawlers import DoubanGallery, DoubanGalleryTopic, DoubanDoulie
from crawlers import DoubanTopicReply
from crawlers import DoubanGroupTopics_2

from crawlers import DoubanProfileStatus, \
                     DoubanProfileSocialNetwork, DoubanProfileNote, \
                     DoubanObject, SaveProfileInfo
from filters import clean_group_topic, ZuFang
from misc.constants import LOGGER_FORMAT, CRAWLER_TYPE, CHROME_DRIVE_PATH, CRAWLER_FOLDER
from misc.utils import logger, timeConsumed, login, LoginBySession



def douban_login(get_object):
    def wrapper(*args, browser=None):
        browser = login(browser)
        get_object(*args, browser=browser)
    return wrapper


@timeConsumed
@douban_login
def get_group_topics(filename_url_dict, browser=None):
# def get_group_topics():
    '''
    获取一个小组下的所有话题
    '''
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_GROUP_TOPICS}'
    topic_reply_dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_TOPIC_REPLY}'
    max_pages = 100
    start_page = 0
    for file_name, url in filename_url_dict.items():
        crawler = DoubanGroupTopics_2(url, browser, start_page, max_pages, dir_path, file_name)
        crawler.start()
        crawler.save()
        df = clean_group_topic(file_name)
        df['an'] = df['an'] + '_' + pd.Series(list(range(df.shape[0]))).map(str)
        filename_url_dict2 = dict(df[['an', 'pl']].values.tolist())
        for file_name1, url1 in filename_url_dict2.items():
            crawler = DoubanTopicReply(url1, browser, 1, topic_reply_dir_path, file_name1)
            crawler.start()
            crawler.save()


@timeConsumed
def get_group_topic_reply(filename_url_dict, browser=None):
    '''
    获取一个小组下一个话题的信息
    '''
    assert browser != None
    topic_reply_dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_TOPIC_REPLY}'
    for file_name, url in filename_url_dict.items():
        crawler = DoubanTopicReply(url, browser, 500, topic_reply_dir_path, file_name)
        crawler.start()
        crawler.save()


@timeConsumed
@douban_login
def get_people_profile(filename_url_dict, browser=None):
    '''
    获取个人主页下的信息
    * note (日志)
    * status (说）
    '''
    people_profile_dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_PEOPLE_PROFILE}'

    for file_name, url in filename_url_dict.items():
        crawler = DoubanProfileStatus(url=url, browser=browser, max_pages=30, \
                                        dir_path=people_profile_dir_path, file_name=file_name)
        crawler.start()
        crawler.save(figures=True, videos=True, parallel=True)


@timeConsumed
@douban_login
def get_movie_review_list(filename_url_dict, browser=None):
    '''
    获取一部电影的长影评
    '''
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_MOVIE_REVIEW_LIST}'
    for file_name, url in filename_url_dict.items():
        crawler = DoubanMovieReviewList(url=url, browser=browser, dir_path=dir_path, file_name=file_name)
        crawler.start()
        crawler.save(csv_json=False)


@timeConsumed
@douban_login
def get_doulie(filename_url_dict, browser=None):
    '''
    依据豆列url 获取一个豆列下的信息
    '''
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_DOULIE}'
    for file_name, url in filename_url_dict.items():
        crawler = DoubanDoulie(url, browser, dir_path, file_name=file_name)
        crawler.start()
        crawler.save()


@timeConsumed
def get_gallery_topics(filename_url_dict, browser=None):
    '''
    获取话题广场下的所有类别下的所有话题
    '''
    gallery_topic_dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_GALLERY_TOPICS}'
    for file_name, url in filename_url_dict.items():
        crawler = DoubanGallery(url=url, browser=browser,
                                dir_path=gallery_topic_dir_path,
                                file_name=file_name)
        crawler.start()
        crawler.save()


@timeConsumed
def get_gallery_topic(filename_url_dict, browser=None):
    '''
    获取一个话题下的所有内容

    模拟持续下拉页面不断加载新的内容，条件控制停止与否
    '''
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_GALLERY_TOPICS}'
    for file_name, url in filename_url_dict.items():
        crawler = DoubanGalleryTopic(url, browser, dir_path, file_name)
        crawler.start()
        crawler.save(csv_json=False)


@timeConsumed
def get_book_list(filename_url_dict, browser=None):
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_BOOK_LIST}'
    for file_name, url in filename_url_dict.items():
        crawler = DoubanBookList(url, browser, dir_path, file_name)
        crawler.start()
        crawler.save()


@timeConsumed
@douban_login
def get_note(filename_url_dict, browser=None):
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_NOTE}'
    for file_name, url in filename_url_dict.items():
        do = DoubanObject(url, browser)
        do.fetch_note()
        tem = SaveProfileInfo(dir_path=dir_path, file_name=file_name)
        tem.new_info.append(do.info)
        tem.picture_urls.update(do.picture_urls)
        tem.video_urls.update(do.video_urls)
        tem.save(figures=True, videos=True, parallel=True)


def clean_group_topics(filenames):
    for group in filenames:
        filename_url_dict = clean_group_topic(group)


def data_analysis_on_profile(filenames):
    from filters.raw_data_processing import cleaner_people_profile
    for file_name in filenames:
        cleaner = cleaner_people_profile(file_name)
        cleaner.get_statistics()


def data_analysis_on_group_topic(filenames):
    from filters.raw_data_processing import cleaner_topic_reply
    for file_name in filenames:
        cleaner = cleaner_topic_reply(file_name)
        df = cleaner.get_statistics(vis=True)
        breakpoint()


def data_analysis_on_group_zufang(group_places_dict):
    for group, places in group_places_dict.items():
        zf = ZuFang(group, places)
        zf.analyse()


def compose_blog_diy():
    from filters.raw_data_processing import compose_blog
    topic_id = 'Fly'
    main_people = ['Fly', 'Ylf']
    import_people = ['日常消遣', '日不落', 'Chinnnnn']
    sensitive_ts_ls = ['2020-09-21 10:14:47', '2020-10-28 07:14:59']
    compose_blog(topic_id, main_people, import_people, sensitive_ts_ls)


@timeConsumed
def get_social_network():
    '''
    获取以一个人为社交网络为起点，往外延伸，找到这个人关注和被关注的人
    '''
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_SOCIAL_NETWORK}'
    file_name = 'Fly'
    browser = webdriver.Chrome(CHROME_DRIVE_PATH)
    def save(data):
        Path(f'{dir_path}/{file_name}').mkdir(parents=True, exist_ok=True)
        pickle.dump(data, open(f"{dir_path}/{file_name}/{file_name}.pkl", 'wb'))

    browser = login(browser)
    data = []
    layer = 0
    starter = '221102936'
    id_set = set(starter)
    people = set([('FLY', f'{starter}')])

    while True:
        item_related = set()
        n = len(people)
        logger.info(f'{n} at level {layer}')
        for i, item in enumerate(people):
            name = item[0]
            logger.info(f'processing {i+1} in {n}: {name}')
            p_id = item[1]
            url = 'https://www.douban.com/people/' + p_id
            crawler = DoubanProfileSocialNetwork(url, browser, 30)
            crawler.start()

            data.append([name, p_id, crawler.followings[0], crawler.followings[1], 
                         crawler.followers[0], crawler.followers[1]])
            f_f = crawler.followings[0] + crawler.followers[0]

            f_f = set(f_f)
            f_f_id = [i[1] for i in f_f if i[1] not in id_set]
            id_set.update(f_f_id)
            item_related.update(f_f)
        people = item_related
        logger.info(data[-100:])

        if layer >= 1:
            save(data)
        layer += 1
    browser.close()



def get_args():
    parser = argparse.ArgumentParser(
        description="获取豆瓣上的数据， 包括:\
                    \n------------------------------------------------------------------------------\
                    \nfetch:\
                    \n1. douban_group_topics (获取小组下面的多个按时间由最新到最早话题)\
                    \n2. douban_group_topic (获取小组下面一个话题的详情)\
                    \n3. douban_people_profile (获取个人主页上的的数据)\
                    \n4. douban_movie_review_list (获取一部电影的长评)\
                    \n5. douban_doulie (获取豆列里的信息)\
                    \n6. douban_gallery_topics (获取话题广场下的所有类别下的所有话题)\
                    \n7. douban_gallery_topic (获取一个话题下的所有内容)\
                    \n8. douban_book_list(获取一个类别下的1000本书籍)\
                    \n9. douban_note(获取一篇日志)\
                    \n------------------------------------------------------------------------------\
                    \nanalyse:\
                    \n1. clean_group_topics (处理douban_group_topics得到的原始数据)\
                    \n2. data_analysis_on_group_topic(处理douban_group_topics得到的原始数据)\
                    \n3. data_analysis_on_profile(处理douban_people_profile得到的原始数据)\
                    \n4. data_analysis_on_group_zufang(租房)\
                    \n5. FLY_DIY(生成Markdown)\
                    \n------------------------------------------------------------------------------\
                    \nusage example: python3 test_new_features.py --type fetch --object 2",
        formatter_class = argparse.RawTextHelpFormatter)

    # Define how a single command-line argument should be parsed， these calls tell the ArgumentParser how to 
    # take the strings on the command line and turn them into objects.
    parser.add_argument("--type", help="输入 fetch or analyse",  required=True)
    parser.add_argument("--object", help="输入序号，若超过一个中间用逗号分隔",  required=True)
    args = parser.parse_args()
    return args