import configparser
from datetime import datetime
# from lxml import etree
import logging
import re
import time

import requests
from selenium.common.exceptions import TimeoutException


def timeConsumed(a_func):
    def wrapTheFunction(*args, **kwargs):
        a = time.time()
        res = a_func(*args, **kwargs)
        b = time.time()
        print(f'{a_func.__name__} takes {round(b-a, 1)} seconds')
        return res
    return wrapTheFunction


def get_logger():
    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
            fmt = '[%(asctime)s] | file: %(filename)-12s | line: %(lineno)d |\n%(message)s',\
            datefmt = "%Y-%m-%d %H:%M:%S")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger

logger = get_logger()


def login(browser):
    url = 'https://www.douban.com'
    config = configparser.ConfigParser()
    config.read('config.ini')
    user = config['douban']['user_name']
    pw = config['douban']['password']

    browser.get(url)
    browser.switch_to.frame(browser.find_elements_by_tag_name('iframe')[0])

    # # 重点2要先点击用账号密码登录的按钮，不然会找不到输入账号和密码的地方
    # bottom1 = browser.find_element_by_xpath('/html/body/div[1]/div[1]/ul[1]/li[2]')
    # bottom1.click()

    # input1 = browser.find_element_by_id('username')
    # input1.clear()
    # input1.send_keys(f'{user}')

    # input2 = browser.find_element_by_id('password')
    # input2.clear()
    # input2.send_keys(f'{pw}')
    
    time.sleep(35)
    bottom = browser.find_element_by_partial_link_text('登录豆瓣')
    bottom.click()
    breakpoint()
    return browser


def handleTimeout(fn):
    def wrapTheFunction(*args, **kwargs):
        flag = True
        while flag:
            try:
                fn(*args, **kwargs)
                flag = False
            except TimeoutException:
                print('meet TimeoutException, fetching again...')
    return wrapTheFunction


def pass_this_saying(latest_history, created_time):
    if latest_history is None:
        return False
    pattern = re.compile("\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}")
    created_time = pattern.findall(created_time)[0]
    latest_history = pattern.findall(latest_history)[0]
    created_time = datetime.strptime(created_time, '%Y-%m-%d %H:%M:%S')
    latest_history = datetime.strptime(latest_history, '%Y-%m-%d %H:%M:%S')
    return (False if created_time > latest_history else True)



class LoginBySession(object):
    """
    使用 request 模拟登录豆瓣，并获取用户名。
    在开发者工具的 network 选项卡中选中 preserve log 选项，然后输入用户名和错误的密码，找到登录时
    处理请求的 URL。
    """ 
    def __init__(self):
        # 设置请求头
        self.headers = {
            'Referer': 'https://accounts.douban.com/passport/login_popup?login_source=anony',
            'Host': 'accounts.douban.com',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36',
        }
        # 使用 requests.Session 模拟登录
        self.session = requests.Session()
        # 请求登录的 URL
        self.post_url = 'https://accounts.douban.com/j/mobile/login/basic'
        # 登录成功之后访问的 URL
        self.logined_url = 'https://accounts.douban.com/passport/setting'

    def login(self):
        """
            Used to simulate login
            @params:
                name - login user name
                password - login password
        """
        config = configparser.ConfigParser()
        config.read('config.ini')
        name = config['douban']['user_name']
        password = config['douban']['password']
        # 登录时传递的表单数据
        post_data = {
            'ck': '',
            'name': name,
            'password': password,
            'remember': False,
            'ticket': ''
        }
        # 使用 session 发起一个 POST 请求，并携带请求头和表单数据
        breakpoint()
        response = self.session.post(self.post_url, data=post_data, headers=self.headers)
        # 如果模拟登录成功
        if response.status_code == 200:
            # 访问登录之后的页面
            response = self.session.get(self.logined_url, headers=self.headers)
            html = etree.HTML(response.text)
            # 获取并打印用户名
            print(html.xpath('//div[@class="account-form-field"]/input/@value')[0])
            url = 'https://www.douban.com/people/221102936/rev_contacts?start=70'
            print(self.session.get(url, headers=self.headers).status_code)