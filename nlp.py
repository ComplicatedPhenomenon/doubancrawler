from collections import Counter, defaultdict
from concurrent.futures import ProcessPoolExecutor, as_completed
import json 
from itertools import combinations
import multiprocessing
import re
import time

import numpy as np
import pickle
import pandas as pd
from gensim.models import Word2Vec
from tqdm import tqdm
import jieba
import jieba.analyse

pd.set_option('display.max_colwidth', None)

def indices_dict(lis):
    d = defaultdict(list)
    for i,(a,b) in enumerate(lis):
        d[a].append(i)
        d[b].append(i)
    return d


def disjoint_indices(lis):
    d = indices_dict(lis)
    sets = []
    while len(d):
        que = set(d.popitem()[1])
        ind = set()
        while len(que):
            ind |= que 
            que = set([y for i in que 
                         for x in lis[i] 
                         for y in d.pop(x, [])]) - ind
        sets += [ind]
    return sets


def disjoint_sets(lis):
    return [set([x for i in s for x in lis[i]]) for s in disjoint_indices(lis)]


def main():
    file = 'data/crawler/douban_group_topic/吾爱/吾爱.json' 
    with open(file, 'r') as f:
        data = json.load(f)

    df = pd.DataFrame(data['comments'])
    df = df.sort_values('reply_time')
    xdf = df[['reply_name', 'reply_id', 'reply_time', 'reply_content', 'quote']].copy()
    xdf.loc[:, 'quote_content'] = xdf['quote'].str['quote_content']
    xdf.loc[:, 'quote_author'] = xdf['quote'].str['quote_author']
    nid = xdf.drop_duplicates(subset=['reply_id', 'reply_name'])[['reply_id', 'reply_name']].values

    for i, row in tqdm(xdf.iterrows()):
        print(i)
        qt = row['reply_content']
        data = []
        data.append((qt, row['reply_time'], row['reply_name']))
        zed = xdf[xdf.quote_content == qt]
        while zed.shape[0]!=0:
            qt = zed['reply_content'].iloc[0]
            data.append((qt, zed['reply_time'].iloc[0], zed['reply_name'].iloc[0]))
            zed = xdf[xdf.quote_content == qt]
        if len(data) > 10:
            print(pd.DataFrame(data))
            time.sleep(10)


def make_seg(arr):
    """
    return a list of segs
    """
    fstop = open('filters/stopwords_cn.txt', 'r',encoding='utf-8',errors='ingnore')
    stopwords = [i.strip() for i in fstop]
    fstop.close()
    res = []
    for sentences in tqdm(arr):
        segs = jieba.cut(sentences)
        segs = [i for i in segs if i not in stopwords and i!=' ' and i !='']
        res.append(segs)
    return res


def make_tfidf_score(words_ls):
    '''
    评估词对于一个文件集或一个语料库中的其中一份文件的重要程度
    '''
    countlist = [Counter(words) for words in words_ls]
    dicts = {}
    for words in words_ls:
        seg = set(words)
        for word in seg:
            if word not in dicts:
                dicts[word] = 0
            dicts[word] += 1
        
    def tfidf(word, count):
        return (count[word] / sum(count.values())) * (np.log(len(words_ls) / (1 + dicts[word])))
    scores = [{word: tfidf(word, count) for word in count} for count in countlist]

    tem = sorted(dicts.items(), key=lambda i: i[1]) 
    
    data = dict([i for i in tem if i[1] > 30 ])
    breakpoint()
    return scores, data


def main2():
    file = 'data/crawler/douban_group_topic/吾爱/吾爱.json' 
    with open(file, 'r') as f:
        data = json.load(f)

    df = pd.DataFrame(data['comments'])
    df = df.sort_values('reply_time')
    df = df.drop_duplicates(subset=['reply_id', 'reply_time'], ignore_index=True)
    df.loc[:, 'quote_content'] = df['quote'].str['quote_content']
    df.loc[:, 'quote_author'] = df['quote'].str['quote_author']
    xdf = df[['reply_name', 'reply_id', 'reply_time', 'reply_content', 'quote_content', 'quote_author']].copy()

    nid = xdf.drop_duplicates(subset=['reply_id', 'reply_name'])[['reply_id', 'reply_name']].values
    xdf = xdf[xdf.reply_content.map(len) < 30].copy()
    xdf.loc[:, 'reply_content'] = xdf.reply_content.str.findall(r'([A-Za-z]+|[\u4E00-\u9FFF]+)').map(''.join)
    texts = xdf['reply_content'].tolist()
    corpus = make_seg(texts)
    # scores = make_tfidf_score(corpus)
    
    x = corpus
    cnt = 0

    ans = []
    pair_score = {}
    for i, j in tqdm(list(combinations(list(range(len(x))), 2))):
        cnt += 1
        cs = longestCommonSubsequence(x[i],x[j])
        dd = score(len(x[i]), len(x[j]), cs)
        pair_score[(i, j)] = dd
        if dd != 0:
            print(x[i])  
            print(x[j])
        if dd >= 0.5:
            print(f'{len(x[i])} {len(x[j])} ', cs, cnt, '✨✨','\n')
            ans.append((i, j))
        elif 0 < dd <= 0.5:
            print(f'{len(x[i])} {len(x[j])} ', cs, cnt, '🥶🥶','\n')
    for v1, v2 in ans:
        print(x[v1])
        print(x[v2])
        print(len(x[v1]), len(x[v2]), longestCommonSubsequence(x[v1], x[v2]))
    breakpoint()

    zed = disjoint_sets(ans)
    zed = sorted(zed, key=lambda i: -len(i))
    flag = len(zed)
    zed = {k:i for i, item in enumerate(zed) for k in item}
    xx = iter(range(flag, 5000))
    col2 = [zed[i] if i in zed else next(xx) for i in range(len(x))]
    col1 = df.drop_duplicates('tem')['tem'].tolist()
    df = pd.DataFrame({'template': col1, 'cid': col2})
    breakpoint()
    return ans
    breakpoint()



    xdf.reply_time = pd.to_datetime(xdf.reply_time)


def print_in_terminal():
    file = 'data/crawler/douban_group_topic/吾爱/吾爱.json' 
    with open(file, 'r') as f:
        data = json.load(f)

    df = pd.DataFrame(data['comments'])
    df = df.sort_values('reply_time')
    df = df.drop_duplicates(subset=['reply_id', 'reply_time'], ignore_index=True)
    df.loc[:, 'quote_content'] = df['quote'].str['quote_content']
    df.loc[:, 'quote_author'] = df['quote'].str['quote_author']
    xdf = df[['reply_name', 'reply_id', 'reply_time', 'reply_content', 'quote_content', 'quote_author']].copy()


    # print(*df[df.reply_name=='连越']['reply_content'].tolist())
    # print(*[i + '\n\n\n\n\n' for i in df[df.reply_name=='连越']['reply_content'].tolist()])
    zed = xdf[xdf.reply_time.dt.date == pd.to_datetime('2021-01-30').date()]
    zed2 = zed[['reply_name', 'reply_time', 'reply_content']]

    space = zed2.reply_name.map(len).max() * 2
    
    for i, row in zed2.iterrows():
        t1 = f'| {row["reply_name"]:26}  | {str(row["reply_time"]):20} | ' 
        t2 = f'| {" ":28}  | {" ":20} | ' 
        print('-'*128)
        
        tem = row['reply_content']
        tem = [tem[i*35: (i+1)*35] for i in range(len(tem)//35 + 1)]
        print(t1 + tem[0])
        for k in tem[1:]:
            k = k.replace('\n', f'\n{t2}')
            print(t2 + k)
    breakpoint()


p = './intermediate_dir/'


class Consumer(multiprocessing.Process):
    def __init__(self, task_queue, result_queue):
        multiprocessing.Process.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue

    def run(self):
        proc_name = self.name
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                # Poison pill means shutdown
                # print('{}: Exiting'.format(proc_name))
                self.task_queue.task_done()
                break
            answer = next_task()
            self.task_queue.task_done()
            self.result_queue.put(answer)


def timeConsumed(a_func):
    def wrapTheFunction(*args, **kwargs):
        a = time.time()
        res = a_func(*args, **kwargs)
        b = time.time()
        print(f'{a_func.__name__} takes {round(b-a, 1)} seconds')
        return res
    return wrapTheFunction


def longestCommonSubsequence(text1, text2) -> int:
    m, n = map(len, (text1, text2))
    if m < n:
        return longestCommonSubsequence(text2, text1)
    dp = [0] * (n + 1)
    for c in text1:
        prevRow, prevRowPrevCol = 0, 0
        for j, d in enumerate(text2):
            prevRow, prevRowPrevCol = dp[j + 1], prevRow
            dp[j + 1] = prevRowPrevCol + 1 if c == d else max(dp[j], prevRow)
    return dp[-1]  


def preprocess_data(df):
    df.loc[:, 'ts'] = pd.to_datetime(df['trigger_time'], format = '%Y%m%d%H%M%S')
    df.loc[:, 'raw_text'] = df.loc[:, 'trigger_content']
    df.loc[:, 'trigger_content'] = df.trigger_content.str.findall('[\u4e00-\u9fffA-Za-z]+')
    df.loc[df.trigger_content==''] = 'empty'
    df.trigger_content = df.trigger_content.fillna('empty')
    df = df[['ts', 'raw_text', 'trigger_content']]
    return df


def score(a, b, c):
    if c == 0: return 0
    rate = max(a, b)/min(a, b)
    if rate > 1.5: return 0
    ans = c / min(a, b)
    if min(a, b) < 20 and ans < 0.6: return 0
    if c < 10 and a!=b and abs(b-a) > 2:
        ans = ans*(1/(abs(b-a)-1))


    elif abs(b-a) > 2 and min(a, b) < 10:
        ans = ans*(1/abs(b-a))
    ans = round(ans, 2)
    return ans


def score2(a, b, c):
    if c < 3: return 0

    rate = max(a, b)/min(a, b)
    if rate > 1.7 and c < 10: return 0
    ans = c / min(a, b)
    ans = round(ans, 2)
    return ans


def a_func(sample, x):
    ans = []
    for i, j in sample:
        cs = longestCommonSubsequence(x[i],x[j])
        dd = score(len(x[i]), len(x[j]), cs)
        if dd >= 0.5:
            ans.append((i, j))
    return ans


class Task:
    def __init__(self, sample, x):
        self.sample = sample
        self.x = x

    def __call__(self):
        ans = []
        for i, j in self.sample:
            cs = longestCommonSubsequence(self.x[i], self.x[j])
            dd = score(len(self.x[i]), len(self.x[j]), cs)
            if dd >= 0.4:
                ans.append((i, j))
        return ans


def multi_processing_fetch(a_func, x):
    futures = []
    size = 10**5
    sample = list(combinations(list(range(len(x))), 2))
    samples = [sample[i*size:(i+1)*size] for i in range(len(sample)//size+1)]
    with ProcessPoolExecutor(128) as executor:
        for sample in tqdm(samples):
            futures.append(executor.submit(a_func, sample, x))
    res = []
    for task in as_completed(futures):
        res += task.result()
    # pickle.dump(res, open(f'{p}' + "group_2.pkl", "wb"))
    return res


@timeConsumed
def main3():
    df = pd.read_csv('监控大半年历史.csv', sep='\t')
    df = preprocess_data(df)
    df.loc[:, 'tem'] = df.trigger_content.map(lambda i: ' '.join(i))
    y = df.drop_duplicates('tem')['trigger_content'].tolist()
    x = [sum([re.findall('([a-zA-Z]+|[^a-zA-Z])', i) for i in z], [])  for z in y]
    multi_processing_fetch(a_func, x)


@timeConsumed
def main4():
    file = 'data/crawler/douban_group_topic/吾爱/吾爱.json' 
    with open(file, 'r') as f:
        data = json.load(f)

    df = pd.DataFrame(data['comments'])
    df = df.sort_values('reply_time')
    df = df.drop_duplicates(subset=['reply_id', 'reply_time'], ignore_index=True)
    df.loc[:, 'quote_content'] = df['quote'].str['quote_content']
    df.loc[:, 'quote_author'] = df['quote'].str['quote_author']
    xdf = df[['reply_name', 'reply_id', 'reply_time', 'reply_content', 'quote_content', 'quote_author']].copy()

    xdf = xdf[xdf.reply_content.map(len) < 10]
    y = xdf['reply_content'].tolist()
    x = [sum([re.findall(r'([A-Za-z]+|[\u4E00-\u9FFF]+)', i) for i in z], [])  for z in y]
    cnt = 0

    ans = []
    pair_score = {}
    for i, j in tqdm(list(combinations(list(range(len(x))), 2))):
        cnt += 1
        cs = longestCommonSubsequence(x[i],x[j])
        dd = score(len(x[i]), len(x[j]), cs)
        pair_score[(i, j)] = dd
        if dd != 0:
            print(x[i])  
            print(x[j])
        if dd >= 0.4:
            print(f'{len(x[i])} {len(x[j])} ', cs, cnt, '✨✨','\n')
            ans.append((i, j))
        elif 0 < dd <= 0.5:
            print(f'{len(x[i])} {len(x[j])} ', cs, cnt, '🥶🥶','\n')
    for v1, v2 in ans:
        print(x[v1])
        print(x[v2])
        print(len(x[v1]), len(x[v2]), longestCommonSubsequence(x[v1], x[v2]))
    breakpoint()

    zed = disjoint_sets(ans)
    zed = sorted(zed, key=lambda i: -len(i))
    flag = len(zed)
    zed = {k:i for i, item in enumerate(zed) for k in item}
    xx = iter(range(flag, 5000))
    col2 = [zed[i] if i in zed else next(xx) for i in range(len(x))]
    col1 = df.drop_duplicates('tem')['tem'].tolist()
    df = pd.DataFrame({'template': col1, 'cid': col2})
    breakpoint()
    return ans


@timeConsumed
def main5():
    df = pd.read_csv('监控大半年历史.csv', sep='\t')
    df = preprocess_data(df)
    df.loc[:, 'tem'] = df.trigger_content.map(lambda i: ' '.join(i))
    y = df.drop_duplicates('tem')['trigger_content'].tolist()
    x = [sum([re.findall('([a-zA-Z]+|[^a-zA-Z])', i) for i in z], [])  for z in y]

    tasks = multiprocessing.JoinableQueue()
    results = multiprocessing.Queue()

    # Start consumers
    num_consumers = 64
    print('Creating {} consumers'.format(num_consumers))
    consumers = [Consumer(tasks, results) for _ in range(num_consumers)]
    for w in consumers:
        w.start()

    # Enqueue jobs
    num_jobs = 0

    size = 10**5
    sample = list(combinations(list(range(len(x))), 2))
    samples = [sample[i*size:(i+1)*size] for i in range(len(sample)//size+1)]
    for sample in samples:
        num_jobs += 1
        tasks.put(Task(sample, x))

    # Add a poison pill for each consumer
    for i in range(num_consumers):
        tasks.put(None)

    # Start printing results
    all_result = []
    for _ in tqdm(range(num_jobs)):
        d = results.get()
        if d:
            all_result += d
    ans = all_result
    pickle.dump(ans, open(f'{p}' + "group_2.pkl", "wb"))
    zed = disjoint_sets(ans)
    zed = sorted(zed, key=lambda i: -len(i))
    flag = len(zed)
    zed = {k:i for i, item in enumerate(zed) for k in item}

    xx = iter(range(flag, 5000))
    col2 = [zed[i] if i in zed else next(xx) for i in range(len(x))]
    col1 = df.drop_duplicates('tem')['tem'].tolist()
    df = pd.DataFrame({'template': col1, 'cid': col2})


    df2 = df[df.cid==0].copy()
    df2.loc[:, 'len'] = df2.template.map(len)
    df2 = df2.sort_values('len')
    x = df2.template.str.findall('([a-zA-Z]+|[^a-zA-Z])').map(lambda item: [i for i in item if i!=' ']).tolist()
    pair = defaultdict(set)
    for i, j in tqdm(list(combinations(list(range(len(x))), 2))):
        flag = 0
        for _, v in pair.items():
            if i in v or j in v:
                flag = 1
                break
        if flag ==1: continue
        cs = longestCommonSubsequence(x[i],x[j])
        dd = score2(len(x[i]), len(x[j]), cs)
        if dd >= 0.25:
            pair[i].add(j)
    
    breakpoint()
    basis = df.cid.max()
    templates = df2.template.tolist()
    tem = [list(v) + [k] for k, v in pair.items()]
    zed2 = {templates[j]:i+basis for i, item in enumerate(tem) for j in item}
    df.loc[:, 'cid'] = df.apply(lambda i: zed2[i['template']] if i['template'] in zed2 else i['cid'], axis=1)
    df.sort_values(['cid', 'template'])[['cid', 'template']].to_csv('res.csv', index=False)
    breakpoint()
   

if __name__ == "__main__":
    # main()
    main2()


