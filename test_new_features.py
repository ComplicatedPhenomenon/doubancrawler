import json

from selenium import webdriver

from misc.constants import CHROME_DRIVE_PATH
from misc.utils import logger
from misc.tool import get_group_topics, get_group_topic_reply, get_people_profile, \
    get_movie_review_list, get_doulie, get_gallery_topics, get_gallery_topic, \
    get_book_list, get_note, clean_group_topics, data_analysis_on_group_topic,\
    data_analysis_on_profile, data_analysis_on_group_zufang, compose_blog_diy, get_args



def main():
    fetch_id_types = {1: "douban_group_topics",
                      2: "douban_group_topic",
                      3: "douban_people_profile",
                      4: "douban_movie_review_list",
                      5: "douban_doulie",
                      6: "douban_gallery_topics",
                      7: "douban_gallery_topic",
                      8: "douban_book_list",
                      9: "douban_note"}

    fetch_fns = [get_group_topics,
                 get_group_topic_reply,
                 get_people_profile,
                 get_movie_review_list,
                 get_doulie,
                 get_gallery_topics,
                 get_gallery_topic,
                 get_book_list,
                 get_note]

    analyse_id_type= {1: "clean_group_topics",
                      2: "data_analysis_on_group_topic",
                      3: "data_analysis_on_profile",
                      4: "data_analysis_on_group_zufang",
                      5: "NoteExisted"}

    analyse_fns = [clean_group_topics,
                   data_analysis_on_group_topic,
                   data_analysis_on_profile,
                   data_analysis_on_group_zufang,
                   compose_blog_diy]

    data = json.load(open('config.json', 'r'))
    action = get_args().type
    objs = get_args().object.split(',')
    objs = list(map(int, objs))


    if action == 'fetch':
        try:
            assert all([0<i<10 for i in objs])
        except AssertionError:
            logger.error('当下只支持1-9的功能')
            return
        # options = webdriver.ChromeOptions()
        # options.headless = True
        # browser = webdriver.Chrome(CHROME_DRIVE_PATH, options=options)
        browser = webdriver.Chrome(CHROME_DRIVE_PATH)
        for i in objs:
            filename_url_dict = data[fetch_id_types[i]]
            try:
                assert len(filename_url_dict) != 0
            except AssertionError:
                logger.error('先设置配置文件!')
                continue
            fetch_fns[i-1](filename_url_dict, browser=browser)
        browser.close()

    elif action == 'analyse':
        try:
            assert all([0<i<6 for i in objs])
        except AssertionError:
            logger.error('当下只支持1-5的功能')
            return

        for i in objs:
            filenames = data.get(analyse_id_type[i], None)
            try:
                if filenames != None:
                    assert len(filenames) != 0
            except AssertionError:
                logger.error('先设置配置文件!')
                continue
            if filenames:
                analyse_fns[i-1](filenames)
            else:
                analyse_fns[i-1]()
    else:
        raise ValueError('Only <fetch> and <analyse> are supported!')


if __name__ == '__main__':
    # get_group_topics()
    main()