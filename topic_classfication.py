import re
from glob import glob
import json
import subprocess


import pandas as pd
from gensim.models import Word2Vec
from tqdm import tqdm
import jieba
import jieba.analyse
import networkx as nx
import pickle
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from graphviz import Digraph

from misc.constants import FONT_PATH, CRAWLER_TYPE, CRAWLER_FOLDER

myfont = FontProperties(fname=FONT_PATH, size=15)


def plot_network(nodes, edges, file_name):
    fig = plt.figure(figsize=(50, 50))
    G = nx.DiGraph()
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    nx.draw(G, font_family='Songti', with_labels=True)
    fig.savefig(f'{file_name}_social_network.svg', format='svg')


def plot_network_2(df, file_name):
    color = cycle(["#FFFF00", "#00F5FF", "#0000CD"])
    sx = 'digraph G {\n  label = "My way to become a astrophysicist";\n  node [shape = record];\n'
    for _, row in df.iterrows():
        sx += '  '+ row['profile_id'].replace('-', '') + ' [label = "' + row['people'] + '"];\n'
        for j in row['followers']:
            sx += '  '+ j[1].replace('-', '') + ' [label = "' + j[0] + '"];\n'
    sx += '\n'
    df.loc[:, 'x'] = df.followers.map(lambda item: [i[1].replace('-', '') for i in item]).map(';'.join)
    edges = (df['profile_id'].replace('-', '') + '->{' + df['x'] + '}\n').values
    for edge in edges:
        sx += '  '+ edge + f'[color="' + next(color) + '"];\n'
    sx += '}'

    with open(f'{file_name}.dot', 'w') as f:
        f.write(sx)


def social_network():
    """
    遍历一个社交网络， 用circo 绘制关系图
    """
    dir_path = f'{CRAWLER_FOLDER}/{CRAWLER_TYPE.DOUBAN_SOCIAL_NETWORK}'
    file_name = 'Fly'
    data = pickle.load(open(f"{dir_path}/{file_name}/{file_name}.pkl", 'rb'))
    df = pd.DataFrame(data)
    df.columns = ['people', 'profile_id', 'followings', 'following_num', 'followers', 'follower_num']
    data = df[['people', 'followings', 'followers']].values.tolist()
    edges = []
    nodes = set()

    for i in data:
        nodes.add(i[0])
        for j in i[1]:
            nodes.add(j[0])
            edges.append((i[0], j[0]))
        for j in i[2]:
            edges.append((j[0], i[0]))
            nodes.add(j[0])

    assert all([len(k)==2 for k in edges])
    # plot_network_2(df[df.follower_num.map(int) > 100].copy(), file_name)
    plot_network_2(df.iloc[5:20,:].copy(), file_name)
    subprocess.Popen(['circo', '-Tsvg', 'Fly.dot', '-o', 'Fly.svg'])


def main():
    from filters.raw_data_processing import compose_blog
    # topic_id = 'Fly'
    # main_people = ['Fly', 'Ylf']
    # import_people = ['日常消遣', '日不落', 'Chinnnnn']
    # sensitive_ts_ls = ['2020-09-21 10:14:47', '2020-10-28 07:14:59']
    # compose_blog(topic_id, main_people, import_people, sensitive_ts_ls)

    fs_ls = glob('data/crawler/douban_group_topic/*/*.json')
    fs_ls = [fs for fs in fs_ls if not re.findall('_\d+', fs)] 
    for file in fs_ls:
        print(file.split('/')[-1])
        with open(file, 'r') as f:
            data = json.load(f)
        df = pd.DataFrame(data['comments'])
        try:
            df = df.sort_values('reply_time')
        except KeyError:
            assert df.shape[0]==0
            continue
        xdf = df[['reply_name', 'reply_id', 'reply_time', 'reply_content', 'quote']].copy()
        xdf.loc[:, 'quote_content'] = xdf['quote'].str['quote_content']
        xdf.loc[:, 'quote_author'] = xdf['quote'].str['quote_author']
        nid = xdf.drop_duplicates(subset=['reply_id', 'reply_name'])[['reply_id', 'reply_name']].values


        topic = file[:-5].split('/')[-1]
        if topic in ['M_Aurelius', '吾爱', 'LTPE', '什么什么小姐', '爸爸我绝不认输', '她不会回来了', '苏晓', 'Je te veux']:
            main_people = []
            import_people = []
            sensitive_ts_ls = []
            compose_blog(topic, main_people, import_people, sensitive_ts_ls)

        # sid = xdf.iloc[0]['reply_id']
        # z = xdf[xdf.reply_id == sid].reply_content.tolist()
        # print(('\n'+'-'*120 + '\n').join(z))
        

if __name__ == "__main__":
    # social_network()
    main()
